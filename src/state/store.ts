import { configureStore } from "@reduxjs/toolkit";
import { cartReducer, loginReducer, locationReducer } from './reducers';

const store = configureStore({
  reducer: {
    auth: loginReducer,
    cart: cartReducer,
    location: locationReducer,
  },
});

export default store;

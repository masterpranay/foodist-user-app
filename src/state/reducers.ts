import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import * as SecureStore from "expo-secure-store";

interface ILoginInitialState {
  id: string;
  mobileNumber: string;
  otp: string;
  status: string;
  otpSendStatus: string;
  otpBufferTime: number;
  email: string;
  name: string;
  otpVerifyStatus: string;
  role?: string;
}

async function save(key: string, value: string) {
  await SecureStore.setItemAsync(key, value);
}

const loginInitialState: ILoginInitialState = {
  id: "",
  mobileNumber: "",
  otp: "",
  status: "",
  otpSendStatus: "not-sent",
  otpBufferTime: 0,
  email: "",
  name: "",
  otpVerifyStatus: "not-started",
  role: "",
};

export const sendOtp = createAsyncThunk(
  "sendOtp",
  async (
    data: {
      mobileNumber: string;
    },
    thunkAPI
  ) => {
    try {
      let user = null;
      // TODO : create a service to send otp
      try {
        console.log("login-user");
        const res = await fetch(
          `${process.env.EXPO_PUBLIC_API_URL}/api/users/login-user`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              mobileNumber: data.mobileNumber,
            }),
          }
        );

        const resData = await res.json();
        user = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      try {
        await fetch(`${process.env.EXPO_PUBLIC_API_URL}/api/users/send-otp`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            mobileNumber: data.mobileNumber,
          }),
        });
      } catch (error: any) {
        throw new Error(error.message);
      }

      thunkAPI.dispatch(setLoginmobileNumber(data.mobileNumber));
      thunkAPI.dispatch(setLoginUserId(user.id));
      thunkAPI.dispatch(setLoginUserRole(user.role));
      thunkAPI.dispatch(setOtpBufferTime(60));

      return { status: "success" };
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);

export const verifyOtp = createAsyncThunk(
  "verifyOtp",
  async (
    data: {
      mobileNumber: string;
      otp: string;
    },
    thunkAPI
  ) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/users/verify-otp`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            mobileNumber: data.mobileNumber,
            otp: data.otp,
          }),
        }
      );

      const resData = await res.json();
      if (resData.message === "Invalid OTP") {
        throw new Error("Invalid OTP");
      }

      if (resData.message === "OTP verified successfully") {
        thunkAPI.dispatch(setLoginStatus("success"));

        await save(
          "user",
          JSON.stringify({
            id: (thunkAPI.getState() as any).auth.id,
            mobileNumber: (thunkAPI.getState() as any).auth.mobileNumber,
            role: (thunkAPI.getState() as any).auth.role,
            status: "loggedin",
          })
        );
      }
      return { status: "success" };
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);

const loginSlice = createSlice({
  name: "SET_LOGIN_PHONE_NUMBER",
  initialState: loginInitialState,
  reducers: {
    setLoginmobileNumber: (state, action) => {
      return { ...state, mobileNumber: action.payload };
    },
    setLoginOtp: (state, action) => {
      return { ...state, otp: action.payload };
    },
    setLoginUserId: (state, action) => {
      return { ...state, id: action.payload };
    },
    setLoginStatus: (state, action) => {
      return { ...state, status: action.payload };
    },
    setLoginUserRole: (state, action) => {
      return { ...state, role: action.payload };
    },
    setOtpBufferTime: (state, action) => {
      return { ...state, otpBufferTime: action.payload };
    },
    setEmail: (state, action) => {
      return { ...state, email: action.payload };
    },
    setName: (state, action) => {
      return { ...state, name: action.payload };
    },
    resetOtpVerifyStatus: (state) => {
      return { ...state, otpVerifyStatus: "not-started" };
    },
    logout: (state) => {
      save(
        "user",
        JSON.stringify({
          id: "",
          mobileNumber: "",
          role: "",
          status: "",
        })
      );

      return {
        ...state,
        id: "",
        mobileNumber: "",
        otp: "",
        status: "",
        otpSendStatus: "not-sent",
        otpBufferTime: 0,
        email: "",
        name: "",
        otpVerifyStatus: "not-started",
      };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(sendOtp.pending, (state, action) => {
      return { ...state, otpSendStatus: "pending" };
    });
    builder.addCase(sendOtp.fulfilled, (state, action) => {
      return { ...state, otpSendStatus: "success" };
    });
    builder.addCase(sendOtp.rejected, (state, action) => {
      return { ...state, otpSendStatus: "failed" };
    });
    builder.addCase(verifyOtp.pending, (state, action) => {
      return { ...state, otpVerifyStatus: "pending" };
    });
    builder.addCase(verifyOtp.fulfilled, (state, action) => {
      return { ...state, otpVerifyStatus: "success" };
    });
    builder.addCase(verifyOtp.rejected, (state, action) => {
      return { ...state, otpVerifyStatus: "failed" };
    });
  },
});

interface ICartItem {
  id: string;
  restaurantId?: string;
  restaurantName?: string;
  price?: number;
  restaurantImage?: string;
  quantity: number;
}

interface ICartInitialState {
  cart: ICartItem[];
}

const cartInitialState: ICartInitialState = {
  cart: [],
};

const cartSlice = createSlice({
  name: "CART",
  initialState: cartInitialState,
  reducers: {
    setCart: (state, action) => {
      return { ...state, cart: action.payload };
    },
    incrementItem: (state, action) => {
      const cart = [...state.cart];
      const itemIndex = cart.findIndex((item) => item.id === action.payload.id);
      if (itemIndex === -1) {
        cart.push({
          id: action.payload.id,
          quantity: 1,
          restaurantId: action.payload.restaurantId,
          price: action.payload.price,
          restaurantName: action.payload.restaurantName,
          restaurantImage: action.payload.restaurantImage,
        });
      } else {
        cart[itemIndex] = {
          ...cart[itemIndex],
          quantity: cart[itemIndex].quantity + 1,
        };
      }

      return { ...state, cart };
    },
    decrementItem: (state, action) => {
      let cart = [...state.cart];
      const itemIndex = cart.findIndex((item) => item.id === action.payload.id);

      if (itemIndex === -1) {
        cart.push({
          id: action.payload.id,
          quantity: 0,
          restaurantId: action.payload.restaurantId,
          price: action.payload.price,
          restaurantName: action.payload.restaurantName,
          restaurantImage: action.payload.restaurantImage,
        });
      } else {
        if (cart[itemIndex].quantity === 1) {
          cart[itemIndex] = {
            ...cart[itemIndex],
            quantity: cart[itemIndex].quantity - 1,
          };
        } else {
          cart[itemIndex] = {
            ...cart[itemIndex],
            quantity: cart[itemIndex].quantity - 1,
          };
        }
      }

      return { ...state, cart };
    },
    // assuming order placed from a restaurant remove all the items from that restaurant
    orderPlaced: (state, action) => {
      const cart = [...state.cart];
      const newCart = cart.filter(
        (item) => item.restaurantId !== action.payload.id
      );
      return { ...state, cart: newCart };
    },
    resetCart: (state) => {
      return { ...state, cart: [] };
    },
  },
});

interface ILocation {
  id: string;
  stateId: string;
  cityId: string;
  houseNumber: string;
  locality: string;
  pincode: string;
  isCurrentLocation: boolean;
  userId: string;
}

interface ILocationInitialState {
  locations: ILocation[];
  fetchStatus?: "loading" | "success" | "failed";
  addStatus?: "loading" | "success" | "failed";
  updateStatus?: "loading" | "success" | "failed";
  deleteStatus?: "loading" | "success" | "failed";
}

const locationInitialState: ILocationInitialState = {
  locations: [],
};

// fetch all location of a user
export const fetchLocations = createAsyncThunk(
  "fetchLocations",
  async (data: { userId: string }, thunkAPI) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/get-all-locations/${data.userId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const resData = await res.json();
      const locations = resData.map((location: any) => {
        return {
          id: location.id,
          stateId: location.stateId,
          cityId: location.cityId,
          houseNumber: location.houseNumber,
          locality: location.locality,
          pincode: location.pincode,
          isCurrentLocation: location.isCurrentLocation,
          userId: location.userId,
        };
      });
      return { status: "success", data: locations };
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);

// add a new location to user
export const addLocation = createAsyncThunk(
  "addLocation",
  async (data: ILocation, thunkAPI) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/add-location`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            stateId: data.stateId,
            cityId: data.cityId,
            houseNumber: data.houseNumber,
            locality: data.locality,
            pincode: data.pincode,
            isCurrentLocation: data.isCurrentLocation,
            userId: data.userId,
          }),
        }
      );

      const resData = await res.json();
      if (resData.message === "Location added successfully") {
        return { status: "success" };
      } else {
        throw new Error(resData.message);
      }
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);

// update a location of a user
export const updateLocation = createAsyncThunk(
  "updateLocation",
  async (
    data: {
      locationIdNew: string;
      locationIdOld: string;
      locationNew: ILocation;
      locationOld: ILocation;
    },
    thunkAPI
  ) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/update-location/${data.locationIdNew}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            stateId: data.locationNew.stateId,
            cityId: data.locationNew.cityId,
            houseNumber: data.locationNew.houseNumber,
            locality: data.locationNew.locality,
            pincode: data.locationNew.pincode,
            isCurrentLocation: "true",
            userId: data.locationNew.userId,
          }),
        }
      );

      const res2 = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/update-location/${data.locationIdOld}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            stateId: data.locationOld.stateId,
            cityId: data.locationOld.cityId,
            houseNumber: data.locationOld.houseNumber,
            locality: data.locationOld.locality,
            pincode: data.locationOld.pincode,
            isCurrentLocation: "false",
            userId: data.locationOld.userId,
          }),
        }
      );

      // const resData1 = await res.json();
      // const resData2 = await res2.json();
      // if (
      //   resData1.message === "Location updated successfully" &&
      //   resData2.message === "Location updated successfully"
      // ) {
      //   return { status: "success" };
      // } else {
      //   throw new Error(resData1.message + resData2.message);
      // }
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);

// delete a location of a user
export const deleteLocation = createAsyncThunk(
  "deleteLocation",
  async (data: { locationId: string }, thunkAPI) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/delete-location/${data.locationId}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const resData = await res.json();
      if (resData.message === "Location deleted successfully") {
        return { status: "success" };
      } else {
        throw new Error(resData.message);
      }
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);

const locationSlice = createSlice({
  name: "LOCATION",
  initialState: locationInitialState,
  reducers: {
    resetLocation: (state) => {
      return { ...state, locations: [] };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchLocations.pending, (state, action) => {
      return { ...state, fetchStatus: "loading" };
    });
    builder.addCase(fetchLocations.fulfilled, (state, action) => {
      return {
        ...state,
        fetchStatus: "success",
        locations: action.payload.data,
      };
    });
    builder.addCase(fetchLocations.rejected, (state, action) => {
      return { ...state, fetchStatus: "failed" };
    });
    builder.addCase(addLocation.pending, (state, action) => {
      return { ...state, addStatus: "loading" };
    });
    builder.addCase(addLocation.fulfilled, (state, action) => {
      return { ...state, addStatus: "success" };
    });
    builder.addCase(addLocation.rejected, (state, action) => {
      return { ...state, addStatus: "failed" };
    });
    builder.addCase(updateLocation.pending, (state, action) => {
      return { ...state, updateStatus: "loading" };
    });
    builder.addCase(updateLocation.fulfilled, (state, action) => {
      return { ...state, updateStatus: "success" };
    });
    builder.addCase(updateLocation.rejected, (state, action) => {
      return { ...state, updateStatus: "failed" };
    });
    builder.addCase(deleteLocation.pending, (state, action) => {
      return { ...state, deleteStatus: "loading" };
    });
    builder.addCase(deleteLocation.fulfilled, (state, action) => {
      return { ...state, deleteStatus: "success" };
    });
    builder.addCase(deleteLocation.rejected, (state, action) => {
      return { ...state, deleteStatus: "failed" };
    });
  },
});

export const {
  setLoginmobileNumber,
  setLoginOtp,
  setLoginUserId,
  setLoginStatus,
  setOtpBufferTime,
  setEmail,
  setName,
  resetOtpVerifyStatus,
  setLoginUserRole,
  logout,
} = loginSlice.actions;
export const loginReducer = loginSlice.reducer;

export const { setCart, incrementItem, decrementItem, orderPlaced, resetCart } =
  cartSlice.actions;
export const cartReducer = cartSlice.reducer;

export const { resetLocation } = locationSlice.actions;
export const locationReducer = locationSlice.reducer;

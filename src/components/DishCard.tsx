import React, { useEffect, useState } from "react";
import { View, Image, Pressable } from "react-native";
import Text from "./Text";
import { Icon, Button } from "@rneui/themed";
import { useDispatch, useSelector } from "react-redux";
import { decrementItem, incrementItem } from "../state/reducers";

type DishCardProps = {
  id: string;
  dishTitle: string;
  price: number;
  description: string;
  type: "veg" | "non-veg";
  cartQuantity: number;
  restaurantId: string;
  restaurantName: string;
  restaurantImage: string;
  openEditDish?: (dish: any) => void;
  image: string;
  category: string;
};

const DishCard: React.FC<DishCardProps> = ({
  id,
  dishTitle,
  price,
  description,
  type,
  cartQuantity = 0,
  restaurantId,
  restaurantName,
  restaurantImage,
  openEditDish,
  image,
  category,
}) => {
  const [quantity, setQuantity] = useState(0);
  const dispatch = useDispatch();
  const cartItem = useSelector((state: any) =>
    state.cart.cart.find((item: any) => item.id === id)
  );
  const role = useSelector((state: any) => state.auth.role);

  useEffect(() => {
    if (cartItem) {
      setQuantity(cartItem.quantity);
    }
  }, [cartItem]);

  return (
    <View className="flex-row py-6 border-b border-dashed border-slate-300">
      <View className="w-3/5 p-4">
        <View className="flex items-center flex-row">
          {type === "veg" ? (
            <View className="bg-green-50 rounded-md border border-green-200 p-0.5">
              <Icon name="circle" color="green" size={12} />
            </View>
          ) : (
            <View className="bg-red-50 rounded-md border border-red-200 p-0">
              <Icon
                name="triangle-up"
                type="entypo"
                color="#cc0000"
                size={16}
              />
            </View>
          )}
          <Text
            className="ml-2 capitalize text-slate-400 text-xs"
            thickness="medium"
          >
            {type}
          </Text>
        </View>
        <Text className="text-base capitalize text-slate-800" thickness="bold">
          {dishTitle}
        </Text>
        <Text className="text-sm text-slate-600 my-1" thickness="bold">
          ₹{price}
        </Text>
        <Text className="text-xs text-gray-500 my-1">{description}</Text>

        {(role === "owner" || role === "admin") && (
          <Pressable
            className="w-24 mt-4"
            onPress={() => {
              openEditDish &&
                openEditDish({
                  id,
                  dishTitle,
                  price,
                  description,
                  type,
                  restaurantId,
                  restaurantName,
                  restaurantImage,
                  image,
                  category,
                });
            }}
          >
            <View className="p-2 px-4 rounded-lg bg-[#01b460]">
              <Text className="text-xs text-white text-center" thickness="bold">
                Edit
              </Text>
            </View>
          </Pressable>
        )}
      </View>
      <View className="w-2/5 flex-1 px-3">
        <Image
          source={{ uri: `${image}` }}
          className="rounded-lg aspect-square"
        />

        {quantity == 0 && role == "user" && (
          <Button
            title={
              <Text className="text-white text-xs" thickness="medium">
                Add
              </Text>
            }
            onPress={() => {
              dispatch(
                incrementItem({
                  id,
                  restaurantId,
                  restaurantName,
                  restaurantImage,
                  price,
                })
              );
            }}
            buttonStyle={{
              backgroundColor: "#01b460",
              marginHorizontal: 16,
              borderRadius: 12,
              borderWidth: 1,
              borderColor: "#01b460",

              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.25,
              shadowRadius: 8,

              elevation: 5,
            }}
            containerStyle={{
              flex: 1,
              marginTop: "-15%",
            }}
          />
        )}

        {quantity > 0 && role == "user" && (
          <View className="flex-row items-center bg-[#01b460] rounded-md mx-4 -mt-5 p-1">
            <Button
              title="-"
              buttonStyle={{
                backgroundColor: "transparent",
                padding: 0,
              }}
              titleStyle={{
                color: "#ffffff",
                fontSize: 16,
              }}
              onPress={() => {
                dispatch(
                  decrementItem({
                    id,
                    restaurantId,
                    restaurantName,
                    restaurantImage,
                    price,
                  })
                );
              }}
            />
            <Text className="text-white text-sm mx-auto" thickness="medium">
              {quantity}
            </Text>
            <Button
              title="+"
              buttonStyle={{
                backgroundColor: "transparent",
                padding: 0,
              }}
              titleStyle={{
                color: "#ffffff",
                fontSize: 16,
              }}
              onPress={() => {
                dispatch(
                  incrementItem({
                    id,
                    restaurantId,
                    restaurantName,
                    restaurantImage,
                    price,
                  })
                );
              }}
            />
          </View>
        )}
      </View>
    </View>
  );
};

export default DishCard;

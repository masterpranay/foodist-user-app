import React, { useEffect } from "react";
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from "react-native";

interface ButtonProps {
  title: string;
  onPress: () => void;
  loading?: boolean;
  disabled?: boolean;
  type?: "primary" | "delete";
}

const Button: React.FC<ButtonProps> = ({
  title,
  onPress,
  loading = false,
  disabled = false,
  type = "primary",
}) => {
  return (
    <TouchableOpacity
      style={
        type === "primary"
          ? disabled
            ? styles.buttonDisabled
            : styles.button
          : disabled
          ? styles.buttonDeleteDisabled
          : styles.buttonDelete
      }
      onPress={onPress}
    >
      {!loading && <Text style={styles.buttonText}>{title}</Text>}
      {loading && <ActivityIndicator color="white" size={"large"} />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#01b460",
    padding: 10,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonDelete: {
    backgroundColor: "rgb(185 28 28)",
    padding: 10,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonDeleteDisabled: {
    backgroundColor: "#ff0000",
    padding: 10,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    opacity: 0.5,
  },
  buttonDisabled: {
    backgroundColor: "#01b460",
    padding: 10,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    opacity: 0.5,
  },
  buttonText: {
    color: "white",
    fontSize: 16,
    fontFamily: "Inter_400Regular",
  },
});

export default Button;

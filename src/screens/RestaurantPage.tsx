import { Icon } from "@rneui/themed";
import { View, Pressable, ActivityIndicator } from "react-native";
import { FlatList, RefreshControl } from "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { DishCard, InputWithIcon, Text } from "../components";
import { useNavigation, useRoute } from "@react-navigation/native";
import { useSelector } from "react-redux";

interface IDish {
  id: string;
  dishTitle: string;
  price: number;
  description: string;
  type: "veg" | "non-veg";
  cartQuantity: number;
  restaurantId: string;
  image: string;
}

interface IRestaurant {
  id: string;
  name: string;
  rating: string;
  categories: [string];
  image?: string;
}

const RestaurantPage = () => {
  const [dishes, setDishes] = useState<IDish[]>([]);
  const [restaurant, setRestaurant] = useState<IRestaurant>({
    id: "",
    name: "",
    rating: "",
    categories: [""],
  });
  const [refreshing, setRefreshing] = useState(false);
  const [input, setInput] = useState("");

  const onChange = (text: string) => {
    setInput(text);
  }

  const route = useRoute();
  const { id } = route.params as { id: string };

  // fetch a single restaurant

  const fetchRestaurant = async () => {
    try {
      setRefreshing(true);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/get-restaurant-by-id/${id}`
      );
      const data = await res.json();
      const restaurant: IRestaurant = {
        id: data.id,
        name: data.name,
        rating: data.rating,
        categories: data.categories.split(","),
      };
      setRestaurant(restaurant);
    } catch (err) {
      console.error(err);
    } finally {
      setRefreshing(false);
    }
  };

  const fetchDishes = async () => {
    try {
      setRefreshing(true);
      const res = await fetch(`${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-all-dishes-id/${id}${input ? `?name=${input}` : ""}`);
      const data = (await res.json()) as [{ id: string }];
      const dishes = await Promise.all(
        data.map(async (item) => {
          const res = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-dish-by-id/${item.id}`
          );
          const data = await res.json();
          return {
            id: data.id,
            dishTitle: data.name,
            price: data.price,
            description: data.description,
            type: data.type,
            cartQuantity: 0,
            restaurantId: data.restaurantId,
            image: data.image,
          };
        })
      );
      setDishes(dishes);
    } catch (err) {
      console.error(err);
    } finally {
      setRefreshing(false);
    }
  };

  useEffect(() => {
    if(refreshing) return;
    async function fetchRestaurantAndDishes() {
      await fetchRestaurant();
      await fetchDishes();
    }
    fetchRestaurantAndDishes();
  }, [input]);

  useEffect(() => {
    if (restaurant && restaurant.rating) {
      if (!(restaurant.rating.split(".").length > 1)) {
        setRestaurant({ ...restaurant, rating: restaurant.rating + ".0" });
      }
    }
  }, [restaurant]);

  const navigation = useNavigation();
  const cartItems = useSelector((state: any) => state.cart.cart);

  const totalCartItems = cartItems.reduce((acc: number, item: any) => {
    if (dishes?.findIndex((dish) => dish.id === item.id) !== -1) {
      return acc + item.quantity;
    } else {
      return acc;
    }
  }, 0);

  return (
    <>
      {(!restaurant || !dishes) && (
        <View className="flex-1 items-center justify-center">
          <ActivityIndicator size="large" color="rgba(200,0,0, 0.8)" />
        </View>
      )}

      {restaurant && dishes && (
        <View className="relative flex-1">
          <View className="flex-row items-center pb-2 px-2 border-b border-slate-200">
            <Pressable
              onPress={() => {
                // @ts-ignore
                navigation.navigate("First");
              }}
            >
              <Icon
                name="left"
                size={32}
                color="#01b460"
                type="ant-design"
              />
            </Pressable>
            <View className="rounded-2xl w-5/6 mx-auto">
              <InputWithIcon
                iconName="search"
                placeholder="Search for dishes"
                onChange={onChange}
                value={input}
              />
            </View>
          </View>

          <View className="flex-col gap-1 items-center justify-center pb-8 pt-4 border-b-8 border-slate-200">
            <Text className="text-xl" thickness="bold">
              {restaurant?.name}
            </Text>
            <View className="flex-row items-center">
              {restaurant?.categories.map((category, index) => (
                <React.Fragment key={index}>
                  <Text
                    key={index}
                    className="text-xs text-slate-400"
                    thickness="medium"
                  >
                    {category}
                  </Text>

                  {restaurant?.categories.length > 1 &&
                    index < restaurant?.categories.length - 1 && (
                      <Icon
                        name="dot-single"
                        size={24}
                        color="rgba(0,0,0,0.2)"
                        type="entypo"
                      />
                    )}
                </React.Fragment>
              ))}
            </View>
            <View className="flex-row items-center pt-2">
              <View className="flex-row items-center p-1 rounded-lg bg-[#24963f]">
                <Text className="text-xs text-white" thickness="bold">
                  {restaurant?.rating}
                </Text>
                <Icon name="star" size={12} color="white" />
              </View>
              <Text className="text-xs ml-4 border-b border-dashed border-slate-200 text-slate-400">
                4.5k ratings
              </Text>
            </View>
          </View>

          {refreshing && (
            <View className="flex-1 items-center justify-center">
              <ActivityIndicator size="large" color="#01b460" />
            </View>
          )}

          {!refreshing && (
            <>
              {dishes.length > 0 && (
                <FlatList
                  data={dishes}
                  renderItem={({ item }) => (
                    <DishCard
                      id={item.id}
                      dishTitle={item.dishTitle}
                      price={item.price}
                      description={item.description}
                      type={item.type}
                      cartQuantity={0}
                      restaurantId={id}
                      restaurantName={restaurant.name}
                      restaurantImage={restaurant.image || ""}
                      image={item.image}
                      category={restaurant.categories.join(",")}
                    />
                  )}
                  keyExtractor={(item) => item.dishTitle}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={fetchDishes}
                    />
                  }
                  ListFooterComponent={<View className="h-12" />}
                />
              )}

              {dishes.length === 0 && (
                <View className="flex-1 items-center justify-center">
                  <Text className="text-3xl font-bold">No Dishes</Text>
                </View>
              )}
            </>
          )}

          {totalCartItems > 0 && (
            <View className="w-full h-12">
              <Pressable
                onPress={() => {
                  // @ts-ignore
                  navigation.navigate("Cart", {
                    restaurantId: id,
                  });
                }}
                className="flex-row items-center justify-center w-full h-full bg-[#01b460] p-2"
              >
                <Text className="text-white text-base mr-4" thickness="bold">
                  {totalCartItems} items added
                </Text>
                <Icon
                  name="arrowright"
                  size={16}
                  color="white"
                  reverseColor="#01b460"
                  type="antdesign"
                  reverse
                />
              </Pressable>
            </View>
          )}
        </View>
      )}
    </>
  );
};

export default RestaurantPage;

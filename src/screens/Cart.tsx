import { Button, Icon } from "@rneui/themed";
import { Text } from "../components";
import { ActivityIndicator, Pressable, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation, useRoute } from "@react-navigation/native";
import { Fragment, useEffect, useState } from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { FlatList } from "react-native-gesture-handler";
import {
  decrementItem,
  incrementItem,
  orderPlaced,
  setLoginStatus,
} from "../state/reducers";
import Toast from "react-native-toast-message";

interface IDish {
  id: string;
  name: string;
  price: number;
  type: string;
  quantity: number;
  incrementQuantity?: () => void;
  decrementQuantity?: () => void;
}

interface ILoginStatus {
  status: boolean | string;
}

const useLoginStatus = (): ILoginStatus => {
  const status = useSelector((state: any) => state.auth.status);

  return {
    status: status,
  };
};

const CartItem = (props: IDish) => {
  const { name, quantity, incrementQuantity, decrementQuantity, price, type } =
    props;

  return (
    <View className="bg-white p-4 rounded-lg mb-4 border-0.5 border-slate-300">
      <View className="flex-row items-center">
        {/* Veg or non - veg icon */}
        {type === "veg" ? (
          <View className="bg-green-50 rounded-md border border-green-200 p-0.5">
            <Icon name="circle" color="green" size={12} />
          </View>
        ) : (
          <View className="bg-red-50 rounded-md border border-red-200 p-0">
            <Icon name="triangle-up" type="entypo" color="#01b460" size={16} />
          </View>
        )}

        {/* Dish name */}
        <Text className="text-base ml-4 text-slate-600" thickness="bold">
          {name}
        </Text>

        {/* Quantity and increment-decrement button */}
        {quantity == 0 && (
          <Button
            title={
              <Text className="text-white text-xs" thickness="medium">
                Add
              </Text>
            }
            onPress={() => {
              // @ts-ignore
              incrementQuantity();
            }}
            buttonStyle={{
              backgroundColor: "#c80000",
              marginHorizontal: 16,
              borderRadius: 12,
              borderWidth: 1,
              borderColor: "#c80000",

              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.25,
              shadowRadius: 8,

              elevation: 5,
            }}
            containerStyle={{
              flex: 1,
              marginTop: "-15%",
            }}
          />
        )}

        {quantity > 0 && (
          <View className="flex-row items-center bg-[#01b460] rounded-md ml-auto">
            <Button
              title="-"
              buttonStyle={{
                backgroundColor: "transparent",
                padding: 0,
              }}
              titleStyle={{
                color: "#ffffff",
                fontSize: 16,
              }}
              onPress={() => {
                // @ts-ignore
                decrementQuantity();
              }}
            />
            <Text className="text-white text-xs mx-auto" thickness="medium">
              {quantity}
            </Text>
            <Button
              title="+"
              buttonStyle={{
                backgroundColor: "transparent",
                padding: 0,
              }}
              titleStyle={{
                color: "#ffffff",
                fontSize: 16,
              }}
              onPress={() => {
                // @ts-ignore
                incrementQuantity();
              }}
            />
          </View>
        )}
      </View>

      <View className="flex-row justify-between mt-2">
        <Text className="text-sm text-slate-400" thickness="medium">
          ₹{price}
        </Text>
        <Text className="text-sm text-slate-400" thickness="medium">
          ₹{price * quantity}
        </Text>
      </View>
    </View>
  );
};

const Header = ({ name, id }: any) => {
  const navigation = useNavigation();

  return (
    <View className="flex-row bg-white p-2">
      <Pressable
        onPress={() => {
          // @ts-ignore
          return navigation.navigate("RestaurantPage", { id });
        }}
      >
        <Icon name="chevron-left" size={32} />
      </Pressable>
      <Text className="text-xl ml-4" thickness="bold">
        {name}
      </Text>
    </View>
  );
};

const CartList = ({ dishes }: { dishes: IDish[] }) => {
  return (
    <View>
      <FlatList
        data={dishes}
        renderItem={({ item }) => {
          return <CartItem {...item} />;
        }}
        keyExtractor={(item) => item.id}
        ListFooterComponent={<View className="h-12" />}
      />
    </View>
  );
};

const AddressBlock = () => {
  return (
    <View className="bg-white p-4 rounded-lg mb-4">
      <Text className="text-base text-slate-600" thickness="bold">
        Delivery Address : Home
      </Text>
      <Text className="text-sm text-slate-400 mt-2" thickness="medium">
        123 Main Street, City, State 123 Main Street, City, State
      </Text>
    </View>
  );
};

const Body = ({ dishes }: { dishes: IDish[] }) => {
  return (
    <View className="bg-slate-50 p-4 h-full">
      <CartList dishes={dishes} />
      <AddressBlock />
    </View>
  );
};

const Footer = ({
  totalPrice,
  postOrder,
}: {
  totalPrice: number;
  postOrder: any;
}) => {
  const { status } = useLoginStatus();
  const dispatch = useDispatch();

  return (
    <View className="absolute w-full bottom-0 flex-row bg-white p-4">
      <View className="flex-row items-center">
        <Text className="text-slate-500 text-base" thickness="bold">
          Total
        </Text>
        <Text className="text-slate-800 text-lg ml-2" thickness="bold">
          ₹{totalPrice}
        </Text>
      </View>

      <Pressable
        onPress={() => {
          if (status !== "loggedin") {
            dispatch(setLoginStatus("login"));
          } else {
            console.log("Order Placed!!");
            Toast.show({
              type: "customSuccess",
              text1: "Order Placed!!",
            });
            postOrder();
          }
        }}
        className="ml-auto"
      >
        <View className="flex-row items-center bg-[#01b460] rounded-md p-2">
          <Text className="text-white text-sm" thickness="bold">
            Place Order
          </Text>
          <Icon name="chevron-right" color="#ffffff" size={32} />
        </View>
      </Pressable>
    </View>
  );
};

const getDish = async (dishId: string, quantity: number) => {
  try {
    const res = await fetch(
      `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-dish-by-id/${dishId}`
    );
    const dishFetched = await res.json();

    const dish = {
      id: dishFetched.id,
      name: dishFetched.name,
      price: dishFetched.price,
      type: dishFetched.type,
      quantity,
    };

    return dish;
  } catch (err) {
    console.error(err);
    return null;
  }
};

const Cart = () => {
  const cartItems = useSelector((state: any) => state.cart.cart);
  const userId = useSelector((state: any) => state.auth.id);
  const [dishes, setDishes] = useState<IDish[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const route = useRoute();
  const { restaurantId } = route.params as any;

  const cartDishes = cartItems.filter(
    (item: any) => item.restaurantId === restaurantId && item.quantity > 0
  );

  const cartDishesIds = cartDishes.map((item: any) => item.id);

  const itemQuantity = cartDishes.reduce((acc: any, item: any) => {
    acc[item.id] = `${item.quantity}`;
    return acc;
  }, {});

  const numberOfItems = cartDishes.reduce(
    (acc: number, item: any) => acc + item.quantity,
    0
  );

  const totalPrice = cartDishes.reduce(
    (acc: number, item: any) => acc + item.price * item.quantity,
    0
  );

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const postOrder = async () => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/orders/create-order`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            dish: cartDishesIds,
            restaurant: restaurantId,
            user: userId,
            totalPrice: totalPrice,
            itemQuantity: itemQuantity,
          }),
        }
      );

      dispatch(
        orderPlaced({
          id: restaurantId,
        })
      );
      // @ts-ignore
      navigation.navigate("First");
    } catch (err) {
      console.error(err);
    }
  };

  const incrementQuantity = (id: string) => {
    dispatch(
      incrementItem({
        id,
        restaurantId,
        restaurantName: cartDishes[0]?.restaurantName,
        restaurantImage: cartDishes[0]?.restaurantImage,
        price: cartDishes.find((item: any) => item.id === id).price,
      })
    );
  };

  const decrementQuantity = (id: string) => {
    dispatch(
      decrementItem({
        id,
        restaurantId,
        restaurantName: cartDishes[0]?.restaurantName,
        restaurantImage: cartDishes[0]?.restaurantImage,
        price: cartDishes.find((item: any) => item.id === id).price,
      })
    );
  };

  // get name, price, type for each dish id

  useEffect(() => {
    if (numberOfItems === 0) {
      // @ts-ignore
      return navigation.navigate("First");
    }

    setLoading(true);
    const fetchDishes = async () => {
      try {
        const dishes = await Promise.all(
          cartDishes.map(async (item: any) => {
            const dish = await getDish(item.id, item.quantity);
            return {
              ...dish,
              incrementQuantity: () => incrementQuantity(dish?.id),
              decrementQuantity: () => decrementQuantity(dish?.id),
            };
          })
        );

        setDishes(dishes);
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    fetchDishes();
  }, [numberOfItems]);

  return (
    <SafeAreaView className="flex-1 bg-white">
      <Header
        name={cartDishes[0]?.restaurantName || "Cart"}
        id={restaurantId}
      />
      {!loading && <Body dishes={dishes} />}
      {!loading && <Footer totalPrice={totalPrice} postOrder={postOrder} />}

      {loading && (
        <ActivityIndicator size="large" color="#01b460" className="my-auto" />
      )}
    </SafeAreaView>
  );
};

export default Cart;

import { SafeAreaView } from "react-native-safe-area-context";
import { RestaurantCard, Text, Button } from "../components";
import { ActivityIndicator, Pressable, View } from "react-native";
import { Fragment, useEffect, useState } from "react";
import {
  FlatList,
  RefreshControl,
  ScrollView,
  TextInput,
} from "react-native-gesture-handler";
import { useDispatch } from "react-redux";
import { logout, setLoginUserRole } from "../state/reducers";
import { BottomSheet } from "@rneui/themed";
import RNPickerSelect from "react-native-picker-select";
import Toast from "react-native-toast-message";

interface Restaurant {
  id: string;
  name: string;
  rating: string;
  categories: string[];
  image: string;
  address: string;
  state: {
    id: string;
    name: string;
  };
  city: {
    id: string;
    name: string;
  };
  pincode: string;
}

const Header = () => {
  const dispatch = useDispatch();

  return (
    <View className="bg-white border-b-0.5 border-gray-200 p-2 flex-row">
      <Text className="text-lg ml-4 text-slate-700" thickness="bold">
        Admin Homepage
      </Text>
      <Pressable
        onPress={() => {
          dispatch(logout());
          dispatch(setLoginUserRole("logout"));
        }}
        className="ml-auto border-b-0.5 border-gray-400 p-1"
      >
        <View>
          <Text>Logout</Text>
        </View>
      </Pressable>
    </View>
  );
};

const Input = ({ label, value, onChange, placeholder }: any) => {
  return (
    <View className="relative my-4">
      <Text className="p-1 bg-white text-slate-400 absolute -top-1/4 left-4 z-10 text-xs">
        {label}
      </Text>
      <TextInput
        placeholder={placeholder}
        className="border-2 border-gray-200 rounded-lg px-4 py-3 text-sm"
        style={{
          fontFamily: "Inter_500Medium",
        }}
        cursorColor={"rgba(0, 0, 0, .5)"}
        value={value}
        onChange={(e) => onChange(e.nativeEvent.text)}
      />
    </View>
  );
};

const EditRestaurant = ({
  isEditRestaurantVisible,
  setIsEditRestaurantVisible,
  isAdding,
  setIsAdding,
  restaurant,
  cities,
  states,
  addRestaurant,
  updateRestaurant,
  deleteRestaurant,
}: {
  isEditRestaurantVisible: boolean;
  setIsEditRestaurantVisible: (value: boolean) => void;
  isAdding: boolean;
  setIsAdding: (value: boolean) => void;
  restaurant: Restaurant;
  cities: {
    stateId: string;
    cities: {
      id: string;
      name: string;
    }[];
  }[];
  states: {
    id: string;
    name: string;
  }[];
  addRestaurant: (restaurant: Restaurant) => void;
  updateRestaurant: (restaurant: Restaurant) => void;
  deleteRestaurant: (restaurantId: string) => void;
}) => {
  const [restaurantData, setRestaurantData] = useState<Restaurant>({
    id: "",
    name: "",
    rating: "",
    categories: [],
    address: "",
    image: "",
    state: {
      id: "",
      name: "",
    },
    city: {
      id: "",
      name: "",
    },
    pincode: "",
  });

  useEffect(() => {
    if (isAdding) {
      setRestaurantData({
        id: "",
        name: "",
        rating: "",
        categories: [],
        address: "",
        image: "",
        state: {
          id: "",
          name: "",
        },
        city: {
          id: "",
          name: "",
        },
        pincode: "",
      });
      return;
    } else {
      setRestaurantData(restaurant);
    }
  }, [restaurant, isAdding]);

  const updateName = (name: string) => {
    setRestaurantData({
      ...restaurantData,
      name,
    });
  };

  const updateRating = (rating: string) => {
    setRestaurantData({
      ...restaurantData,
      rating,
    });
  };

  const updateCategories = (categories: string) => {
    setRestaurantData({
      ...restaurantData,
      categories: categories.split(","),
    });
  };

  const updateAddress = (address: string) => {
    setRestaurantData({
      ...restaurantData,
      address,
    });
  };

  const updateImage = (image: string) => {
    setRestaurantData({
      ...restaurantData,
      image,
    });
  };

  const updatePincode = (pincode: string) => {
    setRestaurantData({
      ...restaurantData,
      pincode,
    });
  };

  return (
    <BottomSheet
      isVisible={isEditRestaurantVisible}
      backdropStyle={{ backgroundColor: "rgba(0, 0, 0, 0.5)" }}
    >
      <View className="relative bg-white flex-1">
        <Text className="text-lg text-[#01b460] p-4 pb-2" thickness="bold">
          {isAdding ? "Add" : "Edit"} Restaurant
        </Text>
        <ScrollView className="bg-white p-4 pt-0 flex-1 max-h-[400] mb-32">
          <View className="flex-col">
            <Input
              label="Name"
              value={restaurantData.name}
              placeholder="Enter Name"
              onChange={updateName}
            />
            <Input
              label="Rating"
              value={restaurantData.rating}
              placeholder="Enter Rating"
              onChange={updateRating}
            />
            <Input
              label="Categories"
              value={restaurantData.categories.join(",")}
              placeholder="Separate by comma"
              onChange={updateCategories}
            />
            <Input
              label="Address"
              value={restaurantData.address}
              placeholder="Enter Address"
              onChange={updateAddress}
            />
            <RNPickerSelect
              value={restaurantData.state.id}
              onValueChange={(value) => {
                setRestaurantData({
                  ...restaurantData,
                  state: {
                    id: value,
                    name:
                      states.find((state) => state.id === value)?.name || "",
                  },
                });
              }}
              items={states.map((state) => {
                return {
                  label: state.name,
                  value: state.id,
                };
              })}
              style={{
                viewContainer: {
                  backgroundColor: "transparent",
                  borderWidth: 2,
                  borderColor: "#e2e8f0",
                  borderRadius: 8,
                },
              }}
            />

            <RNPickerSelect
              value={restaurantData.city.id}
              onValueChange={(value) => {
                setRestaurantData({
                  ...restaurantData,
                  city: {
                    id: value,
                    name:
                      cities
                        .find(
                          (city) => city.stateId === restaurantData.state.id
                        )
                        ?.cities.find((city) => city.id === value)?.name || "",
                  },
                });
              }}
              items={
                cities
                  .find((city) => city.stateId === restaurantData.state.id)
                  ?.cities.map((city) => {
                    return {
                      label: city.name,
                      value: city.id,
                    };
                  }) || []
              }
              style={{
                viewContainer: {
                  backgroundColor: "transparent",
                  borderWidth: 2,
                  borderColor: "#e2e8f0",
                  borderRadius: 8,
                  marginVertical: 16,
                },
              }}
            />

            <Input
              label="Pincode"
              value={restaurantData.pincode}
              placeholder="Enter Pincode"
              onChange={updatePincode}
            />

            <Input
              label="Image"
              value={restaurantData.image}
              placeholder="Enter Image URL"
              onChange={updateImage}
            />
            <View className="h-8"></View>
          </View>
        </ScrollView>
        <View className="absolute w-full bottom-0 bg-white p-4 border-t border-gray-200">
          <View className="flex-row flex-1">
            <Pressable
              onPress={() => {
                setIsEditRestaurantVisible(false);
                isAdding
                  ? addRestaurant(restaurantData)
                  : updateRestaurant(restaurantData);
              }}
              className="border-0.5 border-gray-400 p-2 rounded bg-[#01b460] flex-1 mr-2"
            >
              <Text className="text-white text-center">Save</Text>
            </Pressable>

            {!isAdding && (
              <Pressable
                onPress={() => {
                  setIsEditRestaurantVisible(false);
                  deleteRestaurant(restaurantData.id);
                }}
                className="border-0.5 border-red-800 p-2 rounded bg-white flex-1 ml-2"
              >
                <Text className="text-center">Delete</Text>
              </Pressable>
            )}
          </View>

          <Pressable
            onPress={() => {
              setIsEditRestaurantVisible(false);
              setIsAdding(false);
            }}
            className="p-2 rounded mt-4 border-0.5 border-gray-400 bg-white"
          >
            <Text className="text-center">Close</Text>
          </Pressable>
        </View>
      </View>
    </BottomSheet>
  );
};

const AllRestaurantsList = ({
  restaurants,
  fetchRestaurants,
  refreshing,
  openEditRestaurant,
}: {
  restaurants: Restaurant[];
  fetchRestaurants: () => void;
  refreshing: boolean;
  openEditRestaurant: () => void;
}) => {
  return (
    <Fragment>
      {restaurants.length > 0 && (
        <FlatList
          data={restaurants}
          renderItem={({ item }) => (
            <RestaurantCard
              item={item}
              openEditRestaurant={openEditRestaurant}
            />
          )}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{ paddingHorizontal: 16 }}
          refreshControl={
            <RefreshControl
              onRefresh={fetchRestaurants}
              refreshing={refreshing}
            />
          }
          ListHeaderComponent={() => (
            <View className="flex-row items-center mb-4 w-full">
              <View className="h-16"></View>
              <View className="border-b border-gray-200 flex-1"></View>
              <Text
                className="mx-4 text-sm tracking-wider text-center uppercase text-slate-400"
                thickness="light"
              >
                All Restaurants
              </Text>
              <View className="border-b border-gray-200 flex-1"></View>
            </View>
          )}
          ListFooterComponent={<View className="h-32" />}
        />
      )}

      {!restaurants.length && !refreshing && (
        <View className="flex-1 items-center justify-center">
          <Text className="text-slate-400 text-lg" thickness="medium">
            No restaurants found
          </Text>
        </View>
      )}
    </Fragment>
  );
};

const Body = ({
  refreshing,
  restaurants,
  fetchRestaurants,
  openEditRestaurant,
}: any) => {
  return (
    <Fragment>
      {refreshing && (
        <View className="flex-1 items-center justify-center mt-16">
          <ActivityIndicator size="large" color="#01b460" />
        </View>
      )}

      {!refreshing && (
        <AllRestaurantsList
          restaurants={restaurants}
          fetchRestaurants={fetchRestaurants}
          refreshing={refreshing}
          openEditRestaurant={openEditRestaurant}
        />
      )}
    </Fragment>
  );
};

const AdminHomepage = () => {
  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);
  const [refreshing, setRefreshing] = useState(false);
  const [isEditRestaurantVisible, setIsEditRestaurantVisible] = useState(false);
  const [isAdding, setIsAdding] = useState(false);

  const [states, setStates] = useState<
    {
      id: string;
      name: string;
    }[]
  >([]);
  const [cities, setCities] = useState<
    {
      stateId: string;
      cities: {
        id: string;
        name: string;
      }[];
    }[]
  >([]);

  const [restaurantData, setRestaurantData] = useState<Restaurant>({
    id: "",
    name: "",
    rating: "",
    categories: [],
    address: "",
    image: "",
    state: {
      id: "",
      name: "",
    },
    city: {
      id: "",
      name: "",
    },
    pincode: "",
  });

  const fetchRestaurants = async () => {
    try {
      setRefreshing(true);
      const response = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/get-all-restaurants-id`
      );
      const restaurantsId = await response.json();
      const restaurants = await Promise.all(
        restaurantsId.map(async (item: { id: string }) => {
          const response = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/get-restaurant-by-id/${item.id}`
          );
          const restaurant = await response.json();
          return {
            id: restaurant.id,
            name: restaurant.name,
            rating: restaurant.rating,
            categories: restaurant.categories.split(","),
            image: restaurant.image,
            address: restaurant.address,
            state: {
              id: restaurant.state,
              name: "",
            },
            city: {
              id: restaurant.city,
              name: "",
            },
            pincode: restaurant.pincode,
          };
        })
      );
      setRestaurants(restaurants);
    } catch (err) {
      console.error(err);
    } finally {
      setRefreshing(false);
    }
  };

  const fetchCities = async (stateId: string) => {
    try {
      const response = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/get-all-cities/${stateId}`
      );
      const cities = await response.json();
      setCities((prev) => [
        ...prev,
        {
          stateId,
          cities: cities.map((city: any) => {
            return {
              id: city.id,
              name: city.name,
            };
          }),
        },
      ]);
    } catch (err) {
      console.error(err);
    }
  };

  const fetchStates = async () => {
    try {
      const response = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/get-all-states`
      );
      const states = await response.json();
      setStates(
        states.map((state: any) => ({ id: state.id, name: state.name })) || []
      );
    } catch (err) {
      console.error(err);
    }
  };

  const addRestaurant = async (restaurant: Restaurant) => {
    try {
      const response = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/create-restaurant`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: restaurant.name,
            rating: restaurant.rating,
            categories: restaurant.categories.join(","),
            image: restaurant.image,
            address: restaurant.address,
            state: restaurant.state.id,
            city: restaurant.city.id,
            pincode: restaurant.pincode,
          }),
        }
      );
      const data = await response.json();
      Toast.show({
        type: "customSuccess",
        text1: "Restaurant added successfully",
      });
      fetchRestaurants();
      console.log(data);
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "Error adding restaurant",
      });
    }
  };

  const updateRestaurant = async (restaurant: Restaurant) => {
    try {
      const response = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/update-restaurant/${restaurant.id}`,
        {
          method: "PAtCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: restaurant.name,
            rating: restaurant.rating,
            categories: restaurant.categories.join(","),
            image: restaurant.image,
            address: restaurant.address,
            state: restaurant.state.id,
            city: restaurant.city.id,
            pincode: restaurant.pincode,
          }),
        }
      );
      const data = await response.json();
      Toast.show({
        type: "customSuccess",
        text1: "Restaurant updated successfully",
      });
      fetchRestaurants();
      console.log(data);
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "Error updating restaurant",
      });
    }
  };

  const deleteRestaurant = async (restaurantId: string) => {
    try {
      const response = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/delete-restaurant/${restaurantId}`,
        {
          method: "DELETE",
        }
      );
      const data = await response.json();
      Toast.show({
        type: "customSuccess",
        text1: "Restaurant deleted successfully",
      });
      fetchRestaurants();
      console.log(data);
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "Error deleting restaurant",
      });
    }
  };

  useEffect(() => {
    if (restaurants.length === 0) return;
    const isStateAndCitiesNameExtracted = restaurants.every(
      (restaurant) =>
        restaurant.state.name !== "" && restaurant.city.name !== ""
    );

    if (isStateAndCitiesNameExtracted) return;

    if (cities.length > 0 && restaurants.length > 0) {
      setRestaurants((prev: any) =>
        prev.map((restaurant: any) => {
          const state = states.find(
            (state) => state.id === restaurant.state.id
          );
          const city = cities
            .find((city) => city.stateId === restaurant.state.id)
            ?.cities.find((city) => city.id === restaurant.city.id);
          return {
            ...restaurant,
            state,
            city,
          };
        })
      );
    }
  }, [restaurants]);

  useEffect(() => {
    if (states.length > 0) {
      states.forEach((state) => {
        fetchCities(state.id);
      });
    }
  }, [states]);

  useEffect(() => {
    fetchRestaurants();
    fetchStates();
  }, []);

  return (
    <SafeAreaView>
      <Header />

      <Pressable
        onPress={() => {
          setIsEditRestaurantVisible(true);
          setIsAdding(true);
        }}
        className="p-2 rounded bg-[#01b460] my-4 mx-8"
      >
        <Text className="text-center text-white">Add Restaurant</Text>
      </Pressable>

      <Body
        restaurants={restaurants}
        refreshing={refreshing}
        fetchRestaurants={fetchRestaurants}
        openEditRestaurant={(item: any) => {
          setRestaurantData(item);
          setIsEditRestaurantVisible(true);
        }}
      />
      <EditRestaurant
        isEditRestaurantVisible={isEditRestaurantVisible}
        setIsEditRestaurantVisible={setIsEditRestaurantVisible}
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        restaurant={restaurantData}
        cities={cities}
        states={states}
        addRestaurant={addRestaurant}
        updateRestaurant={updateRestaurant}
        deleteRestaurant={deleteRestaurant}
      />
    </SafeAreaView>
  );
};

export default AdminHomepage;

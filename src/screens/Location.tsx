import React, { useCallback, useEffect, useState } from "react";
import { Pressable, View } from "react-native";
import { Button, InputWithIcon, Text } from "../components";
import { Icon } from "@rneui/themed";
import { useNavigation, useRoute } from "@react-navigation/native";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import RNPickerSelect from "react-native-picker-select";
import { useDispatch, useSelector } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { fetchLocations } from "../state/reducers";
import Toast from "react-native-toast-message";

interface IState {
  id: number;
  name: string;
}

const useFetchStates = () => {
  const [states, setStates] = useState<IState[]>([]);
  const [loading, setLoading] = useState(true);

  const fetchStates = useCallback(async () => {
    setLoading(true);
    const res = await fetch(
      `${process.env.EXPO_PUBLIC_API_URL}/api/location/get-all-states`
    );
    const data = await res.json();

    setStates(
      data.map((state: IState) => ({ id: state.id, name: state.name }))
    );
    setLoading(false);
  }, []);

  useEffect(() => {
    fetchStates();
  }, []);

  return { states, loading, fetchStates };
};

const useFetchCities = () => {
  const [cities, setCities] = useState<IState[]>([]);
  const [loading, setLoading] = useState(true);

  const fetchCities = useCallback(async (stateId: string) => {
    setLoading(true);
    const res = await fetch(
      `${process.env.EXPO_PUBLIC_API_URL}/api/location/get-all-cities/${stateId}`
    );
    const data = await res.json();

    setCities(data.map((city: IState) => ({ id: city.id, name: city.name })));
    setLoading(false);
  }, []);

  return { cities, loading, fetchCities };
};

const useAddLocation = () => {
  const [loading, setLoading] = useState(false);

  const locationFromState = useSelector(
    (state: any) => state.location.locations
  );
  const addStatus = useSelector((state: any) => state.location.addStatus);
  const updateStatus = useSelector((state: any) => state.location.updateStatus);

  const [locations, setLocations] = useState(locationFromState);
  const [currentLocation, setCurrentLocation] = useState<any>(null);

  useEffect(() => {
    if (locations.length > 0) {
      const currentLocation = locations.find(
        (location: any) => location.isCurrentLocation
      );
      setCurrentLocation(currentLocation);
    }
  }, [locations]);

  useEffect(() => {
    setLocations(locationFromState);
  }, [locationFromState]);

  const addLocation = useCallback(async (location: any) => {
    setLoading(true);
    const res = await fetch(
      `${process.env.EXPO_PUBLIC_API_URL}/api/location/add-location`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(location),
      }
    );
    const data = await res.json();
    setLoading(false);
    return data;
  }, []);

  const updateLocation = useCallback(
    async (location: any, locationId: string) => {
      setLoading(true);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/location/update-location/${locationId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(location),
        }
      );
      const data = await res.json();
      console.log(data);
      setLoading(false);
      return data;
    },
    []
  );

  return {
    loading,
    addLocation,
    updateLocation,
    currentLocation,
    addStatus,
    updateStatus,
  };
};

const StatesInput = ({
  label,
  values,
  placeholder,
  value,
  onChange,
}: {
  label: string;
  values: any;
  placeholder: string;
  value?: any;
  onChange?: any;
}) => {
  return (
    <View className="relative my-4">
      <Text className="p-1 bg-white text-slate-400 absolute -top-1/4 left-4 z-10">
        {label}
      </Text>
      <RNPickerSelect
        placeholder={{
          label: placeholder,
          value: null,
        }}
        style={{
          viewContainer: {
            backgroundColor: "transparent",
            borderWidth: 2,
            borderColor: "#e2e8f0",
            borderRadius: 8,
          },
          inputAndroid: {
            fontFamily: "Inter_400Regular",
            fontSize: 16,
            color: "black",
            padding: 16,
            backgroundColor: "transparent",
            borderWidth: 2,
            borderColor: "#e2e8f0",
            borderRadius: 8,
          },
          inputIOS: {
            fontFamily: "Inter_400Regular",
            fontSize: 16,
            color: "black",
            padding: 16,
            backgroundColor: "transparent",
            borderWidth: 2,
            borderColor: "#e2e8f0",
            borderRadius: 8,
          },
        }}
        onValueChange={onChange}
        useNativeAndroidPickerStyle={false}
        items={values.map((value: IState) => ({
          label: value.name,
          value: value.id,
        }))}
        value={value}
        onOpen={() => {}}
      />
    </View>
  );
};

const CityInput = ({
  label,
  values,
  placeholder,
  value,
  onChange,
}: {
  label: string;
  values: any;
  placeholder: string;
  value?: any;
  onChange?: any;
}) => {
  return (
    <View className="relative my-4">
      <Text className="p-1 bg-white text-slate-400 absolute -top-1/4 left-4 z-10">
        {label}
      </Text>
      <RNPickerSelect
        placeholder={{
          label: placeholder,
          value: null,
        }}
        style={{
          viewContainer: {
            backgroundColor: "transparent",
            borderWidth: 2,
            borderColor: "#e2e8f0",
            borderRadius: 8,
          },
          inputAndroid: {
            fontFamily: "Inter_400Regular",
            fontSize: 16,
            color: "black",
            padding: 16,
            backgroundColor: "transparent",
            borderWidth: 2,
            borderColor: "#e2e8f0",
            borderRadius: 8,
          },
          inputIOS: {
            fontFamily: "Inter_400Regular",
            fontSize: 16,
            color: "black",
            padding: 16,
            backgroundColor: "transparent",
            borderWidth: 2,
            borderColor: "#e2e8f0",
            borderRadius: 8,
          },
        }}
        onValueChange={onChange}
        useNativeAndroidPickerStyle={false}
        items={values.map((value: IState) => ({
          label: value.name,
          value: value.id,
        }))}
        value={value}
      />
    </View>
  );
};

const LocationInput = ({
  label,
  placeholder,
  value,
  onChange,
}: {
  label: string;
  placeholder: string;
  value?: any;
  onChange?: any;
}) => {
  return (
    <View className="relative my-4">
      <Text className="p-1 bg-white text-slate-400 absolute -top-1/4 left-4 z-10">
        {label}
      </Text>
      <TextInput
        placeholder={placeholder}
        className="border-2 border-gray-200 rounded-lg px-4 py-3.5 text-base"
        style={{
          fontFamily: "Inter_400Regular",
        }}
        cursorColor={"rgba(0, 0, 0, .5)"}
        value={value}
        onChangeText={onChange}
      />
    </View>
  );
};

const Location = () => {
  const [locationName, setLocationName] = useState({
    houseNumber: "",
    locality: "",
    city: "",
    state: "",
    pincode: "",
  });
  const navigation = useNavigation();
  const dispatchThunk = useDispatch<ThunkDispatch<any, any, any>>();
  const route = useRoute();

  const locationId = (route.params as any)?.locationId || null;

  const { states, loading: loadingStates, fetchStates } = useFetchStates();
  const { cities, fetchCities, loading: loadingCities } = useFetchCities();
  const {
    loading: loadingAddLocation,
    addLocation,
    updateLocation,
    currentLocation,
    addStatus,
    updateStatus,
  } = useAddLocation();

  const locations = useSelector((state: any) => state.location.locations);
  const userId = useSelector((state: any) => state.auth.id);

  useEffect(() => {
    if (locationId) {
      const location = locations.find(
        (location: any) => location.id === locationId
      );
      setLocationName({
        houseNumber: location.houseNumber,
        locality: location.locality,
        city: location.cityId,
        state: location.stateId,
        pincode: location.pincode,
      });
    }
  }, [locationId]);

  const handleHouseNumberChange = (text: string) => {
    setLocationName({ ...locationName, houseNumber: text });
  };

  const handleLocalityChange = (text: string) => {
    setLocationName({ ...locationName, locality: text });
  };

  const handlePincodeChange = (text: string) => {
    setLocationName({ ...locationName, pincode: text });
  };

  const handleCityChange = (text: string) => {
    setLocationName({ ...locationName, city: text });
  };

  const handleStateChange = (text: string) => {
    setLocationName({ ...locationName, state: text });
    fetchCities(text);
  };

  const handleAddLocation = async () => {
    if (
      !locationName.houseNumber ||
      !locationName.locality ||
      !locationName.city ||
      !locationName.state ||
      !locationName.pincode
    ) {
      return;
    }

    const data1 = await addLocation({
      houseNumber: locationName.houseNumber,
      locality: locationName.locality,
      cityId: locationName.city,
      stateId: locationName.state,
      pincode: locationName.pincode,
      isCurrentLocation: true,
      userId: userId,
    });

    let data2 = null;
    if (locations.length > 0 && currentLocation) {
      data2 = await updateLocation(
        {
          houseNumber: currentLocation.houseNumber,
          locality: currentLocation.locality,
          cityId: currentLocation.cityId,
          stateId: currentLocation.stateId,
          pincode: currentLocation.pincode,
          isCurrentLocation: false,
          userId: userId,
        },
        currentLocation.id
      );
    }

    if (data1?.error || data2?.error) {
      Toast.show({
        type: "customError",
        text1: "Some error occured",
      });
    } else {
      Toast.show({
        type: "customSuccess",
        text1: "Location added successfully",
      });
    }

    dispatchThunk(
      fetchLocations({
        userId: userId,
      })
    );
  };

  const handleUpdateLocation = async () => {
    if (
      !locationName.houseNumber ||
      !locationName.locality ||
      !locationName.city ||
      !locationName.state ||
      !locationName.pincode
    ) {
      return;
    }

    const data = await updateLocation(
      {
        houseNumber: locationName.houseNumber,
        locality: locationName.locality,
        cityId: locationName.city,
        stateId: locationName.state,
        pincode: locationName.pincode,
        isCurrentLocation: locations.find(
          (location: any) => location.id === locationId
        ).isCurrentLocation,
        userId: userId,
      },
      locationId
    );

    if (data?.error) {
      Toast.show({
        type: "customError",
        text1: "Some error occured",
      });
    } else {
      Toast.show({
        type: "customSuccess",
        text1: "Location updated successfully",
      });
    }

    dispatchThunk(
      fetchLocations({
        userId: userId,
      })
    );
  };

  return (
    <View className={`flex-1 relative`}>
      <View className="flex-row items-center pb-2 px-2 border-b border-slate-200 z-20 bg-white">
        <Pressable
          onPress={() => {
            // @ts-ignore
            navigation.navigate("First");
          }}
        >
          <Icon name="left" size={32} color="#01b460" type="ant-design" />
        </Pressable>
        <View className="rounded-2xl w-5/6 mx-auto">
          <Text className="text-xl text-[#01b460]" thickness="bold">
            {locationId ? "Update Location" : "Add Location"}
          </Text>
        </View>
      </View>
      <ScrollView className="flex-col gap-4 p-8">
        <LocationInput
          label="House No."
          placeholder="Enter house no."
          value={locationName.houseNumber}
          onChange={handleHouseNumberChange}
        />
        <LocationInput
          label="Locality"
          placeholder="Enter locality"
          value={locationName.locality}
          onChange={handleLocalityChange}
        />
        <StatesInput
          label="State"
          placeholder="Select state"
          values={states}
          value={locationName.state}
          onChange={handleStateChange}
        />

        <CityInput
          label="City"
          placeholder="Select city"
          values={cities}
          value={locationName.city}
          onChange={handleCityChange}
        />

        <LocationInput
          label="Pin Code"
          placeholder="Enter pin code"
          value={locationName.pincode}
          onChange={handlePincodeChange}
        />

        <View className="h-32"></View>
      </ScrollView>
      <View className="absolute bottom-0 w-full px-4 pb-4">
        <Button
          title={locationId ? "Update Location" : "Add Location"}
          onPress={locationId ? handleUpdateLocation : handleAddLocation}
          loading={loadingAddLocation}
        />
      </View>
    </View>
  );
};

export default Location;

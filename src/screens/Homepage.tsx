import { BottomSheet, ButtonGroup, CheckBox, Icon } from "@rneui/themed";
import {
  View,
  TouchableWithoutFeedback,
  Keyboard,
  ActivityIndicator,
  Image,
  Pressable,
  NativeSyntheticEvent,
  TextInputChangeEventData,
} from "react-native";
import {
  FlatList,
  GestureHandlerRootView,
  RefreshControl,
  ScrollView,
} from "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { RestaurantCard, InputWithIcon, Text, Button } from "../components";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import {
  fetchLocations,
  setLoginStatus,
  updateLocation,
} from "../state/reducers";
import { ThunkDispatch } from "redux-thunk";
import Toast from "react-native-toast-message";

interface Restaurant {
  id: string;
  name: string;
  rating: string;
  categories: [string];
  image: string;
  address: string;
  state: {
    id: string;
    name: string;
  };
  city: {
    id: string;
    name: string;
  };
  pincode: string;
}

const AllRestaurantsList = ({
  restaurants,
  fetchRestaurants,
  refreshing,
}: {
  restaurants: Restaurant[];
  fetchRestaurants: () => void;
  refreshing: boolean;
}) => {
  return (
    <React.Fragment>
      {restaurants.length > 0 && (
        <FlatList
          data={restaurants}
          renderItem={({ item }) => <RestaurantCard item={item} />}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{ paddingHorizontal: 16 }}
          refreshControl={
            <RefreshControl
              onRefresh={fetchRestaurants}
              refreshing={refreshing}
            />
          }
          ListHeaderComponent={() => (
            <View className="flex-row items-center mb-4 w-full">
              <View className="border-b border-gray-200 flex-1"></View>
              <Text
                className="mx-4 text-sm tracking-wider text-center uppercase text-slate-400"
                thickness="light"
              >
                All Restaurants
              </Text>
              <View className="border-b border-gray-200 flex-1"></View>
            </View>
          )}
          ListFooterComponent={() => <View className="h-32"></View>}
        />
      )}

      {!restaurants.length && !refreshing && (
        <View className="flex-1 items-center justify-center">
          <Text className="text-slate-400 text-lg" thickness="medium">
            No restaurants found
          </Text>
        </View>
      )}
    </React.Fragment>
  );
};

const useGetCart = () => {
  const cart = useSelector((state: any) => state.cart);
  const uniqueRestaurants: string[] = [
    ...new Set(cart.cart.map((item: any) => item.restaurantId as string)),
  ] as string[];
  const restaurantCount = uniqueRestaurants.length;

  // categories each cart item by restaurantId
  const cartByRestaurant = uniqueRestaurants
    .map((restaurantId) => {
      const items = cart.cart.filter(
        (item: any) => item.restaurantId === restaurantId && item.quantity > 0
      );
      if (items.length === 0) return [];
      return {
        name: items[0].restaurantName,
        id: restaurantId,
        numberOfItems: items.reduce((acc: number, item: any) => {
          return acc + item.quantity;
        }, 0),
        image: items[0].restaurantImage,
        price: items.reduce((acc: number, item: any) => {
          return acc + item.price * item.quantity;
        }, 0),
      };
    })
    .filter((item: any) => item.length !== 0);

  return { cartByRestaurant };
};

const useGetLocations = () => {
  const locationsState = useSelector((state: any) => state.location.locations);
  const [locations, setLocations] = useState<any>(
    useSelector((state: any) => state.location.locations)
  );
  const userId = useSelector((state: any) => state.auth.id);
  const status = useSelector((state: any) => state.location.fetchStatus);
  const updateStatus = useSelector((state: any) => state.location.updateStatus);
  const [updateLoading, setUpdateLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [currentLocation, setCurrentLocation] = useState<any>({});

  const dispatchThunk = useDispatch<ThunkDispatch<any, any, any>>();

  useEffect(() => {
    if (userId === "") return;
    setLoading(true);
    dispatchThunk(
      fetchLocations({
        userId,
      })
    );
  }, [userId]);

  useEffect(() => {
    console.log("update status", updateStatus);
    if (updateStatus === "") return;

    if (updateStatus === "loading") {
      setUpdateLoading(true);
    }

    if (updateStatus === "success") {
      setUpdateLoading(false);
      dispatchThunk(
        fetchLocations({
          userId,
        })
      );
    }
  }, [updateStatus]);

  useEffect(() => {
    if (status === "") return;

    if (status === "loading") {
      setLoading(true);
    }

    if (status === "success" || status === "failed") {
      setLoading(false);
    }
  }, [status]);

  useEffect(() => {
    if (locations.length === 0) return;
    setCurrentLocation(
      locations.find((location: any) => location.isCurrentLocation)
    );
  }, [locations]);

  useEffect(() => {
    setLocations(locationsState);
  }, [locationsState]);

  return {
    locations,
    loading,
    status,
    currentLocation,
    setCurrentLocation,
    setLocations,
    updateLoading,
  };
};

const Homepage = () => {
  const { cartByRestaurant }: any = useGetCart();
  const [showAllRestaurantCartItems, setShowAllRestaurantCartItems] =
    useState(false);
  const [input, setInput] = useState("");
  const [locationBottomSheetVisible, setLocationBottomSheetVisible] =
    useState(false);

  const {
    locations,
    setLocations,
    loading: loadingLocations,
    status,
    currentLocation,
    setCurrentLocation,
    updateLoading,
  } = useGetLocations();

  const dispatchThunk = useDispatch<ThunkDispatch<any, any, any>>();
  const dispatch = useDispatch();

  const loginStatus = useSelector((state: any) => state.auth.status);

  const onChange = (e: any) => {
    setInput(e);
  };

  const navigation = useNavigation();

  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);
  const [refreshing, setRefreshing] = useState(false);

  const fetchRestaurants = async () => {
    try {
      setRefreshing(true);
      const response = await fetch(
        `${
          process.env.EXPO_PUBLIC_API_URL
        }/api/restaurants/get-all-restaurants-id${
          input !== "" ? `?name=${input}` : ""
        }`
      );
      const restaurantsId = await response.json();
      const restaurants = await Promise.all(
        restaurantsId.map(async (item: { id: string }) => {
          const response = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/get-restaurant-by-id/${item.id}`
          );
          const restaurant = await response.json();
          return {
            id: restaurant.id,
            name: restaurant.name,
            rating: restaurant.rating,
            categories: restaurant.categories.split(","),
            image: restaurant.image,
          };
        })
      );
      setRestaurants(restaurants);
    } catch (err) {
      console.error(err);
    } finally {
      setRefreshing(false);
    }
  };

  useEffect(() => {
    if (refreshing) return;
    fetchRestaurants();
  }, [input]);

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View className="flex-1 relative">
        <View className="py-2 px-4 bg-white">
          <Pressable
            onPress={() => {
              if (loginStatus !== "loggedin") {
                Toast.show({
                  type: "customSuccess",
                  text1: "Please login to continue",
                });
                dispatch(setLoginStatus("login"));
                return;
              }
              setLocationBottomSheetVisible(true);
            }}
          >
            <View className="flex-row items-center gap-2">
              <Icon name="location-pin" size={32} color="#01b460" />

              <View>
                <View className="flex-row items-center">
                  <Text className="text-base" thickness="extra-bold">
                    Location
                  </Text>
                  <Icon name="chevron-small-down" size={24} type="entypo" />
                </View>

                {loadingLocations && (
                  <ActivityIndicator size="small" color="#01b460" />
                )}

                {!loadingLocations && (
                  <Text className="text-slate-400 text-xs" thickness="medium">
                    {locations.length > 0
                      ? currentLocation?.locality
                      : "No locations found"}
                  </Text>
                )}
              </View>
            </View>
          </Pressable>

          <View className="rounded-2xl mt-1">
            <InputWithIcon
              iconName="search"
              placeholder="Search for restaurants"
              onChange={onChange}
              value={input}
            />
          </View>
        </View>
        {refreshing && (
          <View className="flex-1 items-center justify-center">
            <ActivityIndicator size="large" color="#01b460" />
          </View>
        )}

        {!refreshing && (
          <AllRestaurantsList
            restaurants={restaurants}
            fetchRestaurants={fetchRestaurants}
            refreshing={refreshing}
          />
        )}

        {cartByRestaurant.length > 0 && (
          <View className="absolute bottom-4 left-4 right-4 shadow-lg border-0.5 border-slate-200 shadow-slate-400 rounded-lg">
            <View className="flex-col items-center bg-white rounded-lg">
              {cartByRestaurant.length > 1 && (
                <Pressable
                  onPress={() => {
                    setShowAllRestaurantCartItems(!showAllRestaurantCartItems);
                  }}
                  className="absolute rounded-full border-0.5 -top-3 left-[47%] border-slate-200 bg-white"
                >
                  <Icon
                    name={`${
                      showAllRestaurantCartItems
                        ? "chevron-small-down"
                        : "chevron-small-up"
                    }`}
                    size={32}
                    type="entypo"
                  />
                </Pressable>
              )}

              <View className="flex-col item-center">
                {showAllRestaurantCartItems ? (
                  <View className="flex-col items-center w-full p-4">
                    {cartByRestaurant.map((item: any, index: number) => (
                      <React.Fragment key={index}>
                        <View
                          className={`flex-row items-center w-full p-4 ${
                            index == cartByRestaurant.length - 1
                              ? "border-b-0"
                              : "border-b-0.5 border-gray-200"
                          }`}
                        >
                          <Image
                            source={{
                              uri: `https://picsum.photos/100/100?random=${index}`,
                            }}
                            className="w-12 h-12 aspect-square rounded-lg"
                          />
                          <View className="flex-col ml-4 mr-auto">
                            <Text className="text-sm" thickness="bold">
                              {item.name}
                            </Text>
                            <Text
                              className="text-xs text-slate-400"
                              thickness="medium"
                            >
                              {item.numberOfItems} items | ₹{item.price}
                            </Text>
                          </View>
                          <Pressable
                            onPress={() => {
                              setShowAllRestaurantCartItems(false);
                              // @ts-ignore
                              navigation.navigate("Cart", {
                                restaurantId: item.id,
                              });
                            }}
                          >
                            <Icon
                              name="chevron-small-right"
                              size={24}
                              type="entypo"
                            />
                          </Pressable>
                        </View>
                        {index < cartByRestaurant.length - 1 && (
                          <View className="border-b border-slate-200 w-full"></View>
                        )}
                      </React.Fragment>
                    ))}
                  </View>
                ) : (
                  <View className="flex-row items-center w-full p-4">
                    <Image
                      source={{
                        uri: `https://picsum.photos/100/100?random=2`,
                      }}
                      className="w-12 h-12 aspect-square rounded-lg"
                    />
                    <View className="flex-col ml-4 mr-auto">
                      <Text className="text-sm" thickness="bold">
                        {cartByRestaurant[0]?.name}
                      </Text>
                      <Text
                        className="text-xs text-slate-400"
                        thickness="medium"
                      >
                        {cartByRestaurant[0]?.numberOfItems} items | ₹
                        {cartByRestaurant[0]?.price}
                      </Text>
                    </View>
                    <Pressable
                      onPress={() => {
                        setShowAllRestaurantCartItems(false);
                        // @ts-ignore
                        navigation.navigate("Cart", {
                          restaurantId: cartByRestaurant[0]?.id,
                        });
                      }}
                    >
                      <Icon
                        name="chevron-small-right"
                        size={24}
                        type="entypo"
                      />
                    </Pressable>
                  </View>
                )}
              </View>
            </View>
          </View>
        )}

        <BottomSheet isVisible={locationBottomSheetVisible}>
          <View className="pt-12">
            <View className="h-96 relative bg-white">
              <Pressable
                className="absolute left-[43%] -top-8"
                onPress={() => {
                  setLocationBottomSheetVisible(false);
                }}
              >
                <Icon
                  name="close"
                  size={24}
                  type="antdesign"
                  color="#01b460"
                  reverse
                />
              </Pressable>

              <View className="pt-8 px-4">
                <View className="flex-row items-center">
                  <Text className="text-lg" thickness="bold">
                    Select Location
                  </Text>
                </View>

                <ScrollView>
                  {locations &&
                    locations.map((item: any, index: number) => (
                      <View key={index} className="flex-row items-center">
                        <CheckBox
                          title={
                            <Text className="text-sm ml-2">
                              {item.locality} {item.houseNumber} {item.city}{" "}
                              {item.state} {item.pinCode}
                            </Text>
                          }
                          checked={item.isCurrentLocation}
                          onPress={() => {
                            if (item.isCurrentLocation) {
                              return;
                            }
                            if (updateLoading) {
                              return;
                            }
                            dispatchThunk(
                              updateLocation({
                                locationIdNew: item.id,
                                locationIdOld: currentLocation.id,
                                locationNew: {
                                  ...item,
                                  isCurrentLocation: "true",
                                },
                                locationOld: {
                                  ...currentLocation,
                                  isCurrentLocation: "false",
                                },
                              })
                            );
                          }}
                        />
                        <Pressable
                          onPress={() => {
                            setLocationBottomSheetVisible(false);
                            // @ts-ignore
                            navigation.navigate("Location", {
                              locationId: item.id,
                            });
                          }}
                        >
                          <Text className="text-sm text-slate-400">Edit</Text>
                        </Pressable>
                      </View>
                    ))}

                  <View className="h-24"></View>
                </ScrollView>
              </View>

              {(updateLoading || loadingLocations) && (
                <View className="absolute top-0 bottom-0 left-0 right-0 bg-white opacity-50 flex items-center justify-center">
                  <ActivityIndicator size="large" color="#01b460" />
                </View>
              )}

              <View className="absolute bottom-0 left-0 right-0 p-4 bg-white">
                <Button
                  title="Add New Location"
                  onPress={() => {
                    setLocationBottomSheetVisible(false);
                    // @ts-ignore
                    navigation.navigate("Location");
                  }}
                />
              </View>
            </View>
          </View>
        </BottomSheet>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Homepage;

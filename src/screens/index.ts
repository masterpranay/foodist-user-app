import Homepage from "./Homepage";
import History from "./History";
import Profile from "./Profile";
import RestaurantPage from "./RestaurantPage";
import Login from "./Login";
import Otp from "./Otp";
import Cart from "./Cart";
import RestaurantOwnerPage from "./RestaurantOwnerPage";
import AdminHomepage from "./AdminHomepage";
import Location from "./Location";

export {
  Homepage,
  History,
  Profile,
  RestaurantPage,
  Login,
  Otp,
  Cart,
  RestaurantOwnerPage,
  AdminHomepage,
  Location,
};

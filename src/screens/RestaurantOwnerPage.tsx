import { SafeAreaView } from "react-native-safe-area-context";
import { Text, DishCard, Button } from "../components";
import { Fragment, useEffect, useState } from "react";
import { ActivityIndicator, Pressable, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation, useRoute } from "@react-navigation/native";
import { BottomSheet, Icon } from "@rneui/themed";
import {
  FlatList,
  RefreshControl,
  ScrollView,
  TextInput,
} from "react-native-gesture-handler";
import { logout } from "../state/reducers";
import RNPickerSelect from "react-native-picker-select";
import Toast from "react-native-toast-message";

interface IDish {
  id: string;
  dishTitle: string;
  price: number;
  description: string;
  type: string;
  cartQuantity: number;
  restaurantId: string;
  image: string;
  category: string;
}

interface IRestaurant {
  id: string;
  name: string;
  rating: string;
  categories: string[];
  address: string;
  image: string;
}

interface Order {
  id: string;
  dishes: {
    id: string;
    name: string;
    quantity: number;
  }[];
  restaurantId: string;
  restaurant: string;
  userId: string;
  user: string;
  mobileNumber: string;
  amount: number;
  status: "pending" | "accepted" | "rejected" | "delivered" | "delivered";
  createdAt: string;
}

const Input = ({ label, value, onChange, placeholder }: any) => {
  return (
    <View className="relative my-4">
      <Text className="p-1 bg-white text-slate-400 absolute -top-1/4 left-4 z-10 text-xs">
        {label}
      </Text>
      <TextInput
        placeholder={placeholder}
        className="border-2 border-gray-200 rounded-lg px-4 py-3 text-sm"
        style={{
          fontFamily: "Inter_500Medium",
        }}
        cursorColor={"rgba(0, 0, 0, .5)"}
        value={value}
        onChange={(e) => onChange(e.nativeEvent.text)}
      />
    </View>
  );
};

const EditDish = ({
  isEditDishVisible,
  setIsEditDishVisible,
  isAddingDish,
  setIsAddingDish,
  dish,
  updateDish,
  addDish,
  deleteDish,
}: {
  isEditDishVisible: boolean;
  setIsEditDishVisible: (value: boolean) => void;
  isAddingDish: boolean;
  setIsAddingDish: (value: boolean) => void;
  dish: IDish;
  updateDish: (dish: IDish) => void;
  addDish: (dish: IDish) => void;
  deleteDish: (dish: IDish) => void;
}) => {
  const [dishData, setDishData] = useState<IDish>({
    id: "",
    dishTitle: "",
    price: 0,
    description: "",
    type: "veg",
    cartQuantity: 0,
    restaurantId: "",
    image: "",
    category: "",
  });

  useEffect(() => {
    if (!isAddingDish) {
      setDishData(dish);
    } else if (isAddingDish) {
      setDishData({
        id: "",
        dishTitle: "",
        price: 0,
        description: "",
        type: "veg",
        cartQuantity: 0,
        restaurantId: "",
        image: "",
        category: "",
      });
    }
  }, [dish, isAddingDish]);

  const updateDishTitle = (text: string) => {
    setDishData({ ...dishData, dishTitle: text });
  };

  const updatePrice = (text: number) => {
    setDishData({ ...dishData, price: text });
  };

  const updateDescription = (text: string) => {
    setDishData({ ...dishData, description: text });
  };

  const updateType = (text: string) => {
    setDishData({ ...dishData, type: text });
  };

  const updateCategory = (text: string) => {
    setDishData({ ...dishData, category: text });
  };

  return (
    <BottomSheet
      isVisible={isEditDishVisible}
      backdropStyle={{ backgroundColor: "rgba(0, 0, 0, 0.5)" }}
    >
      <ScrollView className="bg-white p-4">
        <Text className="text-base text-slate-800" thickness="bold">
          {isAddingDish ? "Add" : "Edit"} Dish
        </Text>
        <View className="my-4 flex-col">
          <Input
            label="Dish Title"
            value={dishData.dishTitle}
            onChange={updateDishTitle}
            placeholder="Enter Dish Title"
          />

          <Input
            label="Price"
            value={dishData.price.toString()}
            onChange={updatePrice}
            placeholder="Enter Price"
          />

          <Input
            label="Description"
            value={dishData.description}
            onChange={updateDescription}
            placeholder="Enter Description"
          />

          <RNPickerSelect
            onValueChange={(value) => updateType(value)}
            items={[
              { label: "Veg", value: "veg" },
              { label: "Non-Veg", value: "non-veg" },
            ]}
            value={dishData.type}
            style={{
              viewContainer: {
                backgroundColor: "transparent",
                borderWidth: 2,
                borderColor: "#e2e8f0",
                borderRadius: 8,
              },
              inputIOS: {
                fontFamily: "Inter_500Medium",
                fontSize: 16,
                paddingVertical: 12,
                paddingHorizontal: 10,
                borderWidth: 2,
                borderColor: "gray",
                borderRadius: 4,
                color: "black",
                paddingRight: 30,
              },
              inputAndroid: {
                fontFamily: "Inter_500Medium",
                fontSize: 16,
                paddingVertical: 12,
                paddingHorizontal: 10,
                borderWidth: 2,
                borderColor: "gray",
                borderRadius: 8,
                color: "black",
                paddingRight: 30,
              },
            }}
          />

          <Input
            label="Category"
            value={dishData.category}
            onChange={updateCategory}
            placeholder="Enter Category"
          />

          <Input
            label="Image"
            value={dishData.image}
            onChange={(text: string) => {
              setDishData({ ...dishData, image: text });
            }}
            placeholder="Enter Image URL"
          />
        </View>

        <View className="flex-row flex-1">
          <Pressable
            onPress={() => {
              if (isAddingDish) {
                addDish(dishData);
              } else {
                updateDish(dishData);
              }
              setIsEditDishVisible(false);
            }}
            className="border-0.5 border-gray-400 p-2 rounded bg-[#01b460] flex-1 mr-2"
          >
            <Text className="text-white text-center">Save</Text>
          </Pressable>

          {!isAddingDish && (
            <Pressable
              onPress={() => {
                setIsEditDishVisible(false);
                deleteDish(dishData);
              }}
              className="border-0.5 border-red-800 p-2 rounded bg-white flex-1 ml-2"
            >
              <Text className="text-center">Delete</Text>
            </Pressable>
          )}
        </View>

        <Pressable
          onPress={() => {
            setIsEditDishVisible(false);
            setIsAddingDish(false);
          }}
          className="border-0.5 border-red-800 p-2 rounded bg-white flex-1 my-4"
        >
          <Text className="text-center">Close</Text>
        </Pressable>

        <View className="h-12" />
      </ScrollView>
    </BottomSheet>
  );
};

const Header = ({ restaurantName }: { restaurantName: string }) => {
  const dispatch = useDispatch();
  const navigate = useNavigation();
  const role = useSelector((state: any) => state.auth.role);

  return (
    <View className="bg-white border-b-0.5 border-gray-200 p-2 flex-row items-center">
      {/* implement the back button */}
      {role == "admin" && (
        <Pressable
          onPress={() => {
            navigate.goBack();
          }}
          className="p-0.5"
        >
          <Icon name="arrow-back" />
        </Pressable>
      )}
      <Text className="text-base ml-4 text-slate-700" thickness="bold">
        {restaurantName}
      </Text>
      <Pressable
        onPress={() => {
          dispatch(logout());
        }}
        className="ml-auto border-b-0.5 border-gray-400 p-0.5"
      >
        <View>
          <Text className="text-slate-600">Logout</Text>
        </View>
      </Pressable>
    </View>
  );
};

const DishesList = ({
  dishes,
  loading,
  fetchDishes,
  restaurant,
  openEditDish,
}: any) => {
  const { id, name } = restaurant;
  return (
    <Fragment>
      {dishes.length > 0 && (
        <FlatList
          data={dishes}
          renderItem={({ item }) => (
            <DishCard
              id={item.id}
              dishTitle={item.dishTitle}
              price={item.price}
              description={item.description}
              type={item.type}
              cartQuantity={0}
              restaurantId={id}
              restaurantName={name}
              restaurantImage={""}
              image={item.image}
              openEditDish={openEditDish}
              category={item.category}
            />
          )}
          keyExtractor={(item) => item.dishTitle}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={fetchDishes} />
          }
          ListFooterComponent={() => {
            return <View className="h-48" />;
          }}
        />
      )}

      {dishes.length === 0 && (
        <View className="flex-1 items-center justify-center">
          <Text className="text-3xl font-bold">No Dishes</Text>
        </View>
      )}
    </Fragment>
  );
};

const Body = ({
  dishes,
  loading,
  fetchDishes,
  restaurant,
  openEditDish,
}: any) => {
  return (
    <Fragment>
      {loading && (
        <View className="flex-1 items-center justify-center mt-16">
          <ActivityIndicator size="large" color="#01b460" />
        </View>
      )}
      {!loading && (
        <DishesList
          dishes={dishes}
          loading={loading}
          fetchDishes={fetchDishes}
          restaurant={restaurant}
          openEditDish={openEditDish}
        />
      )}
    </Fragment>
  );
};

const useGetRestaurantsOrders = (restaurantId: string) => {
  const [orders, setOrders] = useState<Order[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [pendingOrders, setPendingOrders] = useState<Order[]>([]);
  const [ongoingOrders, setOngoingOrders] = useState<Order[]>([]);

  const fetchOrders = async () => {
    console.log("restaurantId", restaurantId);
    try {
      setLoading(true);
      let ordersId = null;
      try {
        const res = await fetch(
          `${process.env.EXPO_PUBLIC_API_URL}/api/orders/get-orders-restaurant/${restaurantId}`
        );
        const resData = await res.json();
        console.log(resData, "resData");
        ordersId = resData;
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let orders: any = null;

      try {
        orders = ordersId.map(async (orderId: any) => {
          const res = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/orders/get-order/${orderId.id}`
          );
          const resData = await res.json();
          return resData;
        });
        orders = await Promise.all(orders);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let dishesName: {
        name: string;
        quantity: number;
      }[][] = [];

      try {
        let dishesNameRes = orders.map(async (order: any) => {
          let tempDishesRes = [];
          tempDishesRes = order.dish.map(async (id: string) => {
            const res = await fetch(
              `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-dish-by-id/${id}`
            );
            const resData = await res.json();
            return {
              id: resData.id,
              name: resData.name,
              quantity: order.itemQuantity[id],
            };
          });
          tempDishesRes = await Promise.all(tempDishesRes);
          return tempDishesRes;
        });

        dishesName = await Promise.all(dishesNameRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let userInfo: {
        id: string;
        mobileNumber: string;
        name: string;
      }[] = [];

      try {
        let userInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/users/get-user-by-id/${order.user}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            mobileNumber: resData.mobileNumber,
            name: resData.name || "No Name",
          };
        });
        userInfo = await Promise.all(userInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      let restaurantInfo: {
        id: string;
        name: string;
      }[] = [];

      try {
        let restaurantInfoRes = orders.map(async (order: any) => {
          const res = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/get-restaurant-by-id/${order.restaurant}`
          );
          const resData = await res.json();
          return {
            id: resData.id,
            name: resData.name,
          };
        });
        restaurantInfo = await Promise.all(restaurantInfoRes);
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }

      orders = orders.map((order: any, index: any) => {
        return {
          id: order.id,
          dishes: dishesName[index],
          restaurant: restaurantInfo[index].name,
          restaurantId: restaurantInfo[index].id,
          userId: userInfo[index].id,
          user: userInfo[index].name,
          mobileNumber: userInfo[index].mobileNumber,
          amount: order.totalPrice,
          status: order.status,
          createdAt: order.created,
        };
      });

      setLoading(false);
      console.log(orders);
      setOrders(orders);
    } catch (error: any) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    if (orders.length > 0) {
      console.log(orders);
      setPendingOrders(orders.filter((order) => order.status === "pending"));
      setOngoingOrders(orders.filter((order) => order.status === "accepted"));
    }
  }, [orders]);

  useEffect(() => {
    console.log("fetching orders");
    if (restaurantId) fetchOrders();
  }, [restaurantId]);

  return { loading, pendingOrders, ongoingOrders, fetchOrders };
};

const OrderItem = ({
  order,
  activeTab,
  acceptOrder,
  rejectOrder,
  dispatchOrder,
}: {
  order: Order;
  activeTab: "pending" | "ongoing";
  acceptOrder: (orderId: string) => void;
  rejectOrder: (orderId: string) => void;
  dispatchOrder: (orderId: string) => void;
}) => {
  // show the user mobileNumber, with dish name and quantity and total amount with date and time
  return (
    <View className="border border-gray-200 p-4 rounded my-2">
      <Text className="text-base" thickness="bold">
        Order Id : {order.id}
      </Text>
      <Text>Dishes : </Text>

      {order.dishes.map((dish: any, index: any) => {
        return (
          <Text key={index} className="text-sm">
            {dish.name} - {dish.quantity}
          </Text>
        );
      })}

      <Text className="text-sm">User : {order.mobileNumber}</Text>

      <Text className="text-sm">Amount : {order.amount}</Text>

      <Text className="text-sm">
        Date :{new Date(order.createdAt).toLocaleDateString()} -
        {new Date(order.createdAt).toLocaleTimeString()}
      </Text>

      {activeTab == "pending" && (
        <View className="flex-row w-full flex-1 mt-4">
          <Pressable
            onPress={() => {
              // accept the order
              acceptOrder(order.id);
            }}
            className="border-0.5 border-green-800 p-2 rounded bg-[#01b460] flex-1 mr-2"
          >
            <Text className="text-center text-white">Accept</Text>
          </Pressable>

          <Pressable
            onPress={() => {
              // reject the order
              rejectOrder(order.id);
            }}
            className="border-0.5 border-red-800 p-2 rounded bg-[#db4242] flex-1 ml-2"
          >
            <Text className="text-center text-white">Reject</Text>
          </Pressable>
        </View>
      )}

      {activeTab == "ongoing" && (
        <Pressable
          onPress={() => {
            // dispatch the order
            dispatchOrder(order.id);
          }}
          className="border-0.5 border-green-800 p-2 rounded bg-[#0152b4] flex-1 my-4"
        >
          <Text className="text-center text-white">Dispatch</Text>
        </Pressable>
      )}
    </View>
  );
};

const Orders = ({ restaurantId }: { restaurantId: string }) => {
  const [activeTab, setActiveTab] = useState<"pending" | "ongoing">("pending");
  const { loading, pendingOrders, ongoingOrders, fetchOrders } =
    useGetRestaurantsOrders(restaurantId);

  const acceptOrder = async (orderId: string) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/orders/accept-order/${orderId}`,
        {
          method: "POST",
        }
      );
      const data = await res.json();
      Toast.show({
        type: "customSuccess",
        text1: "You have accepted the order successfully",
      });
      console.log(data);
      fetchOrders();
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "There was an error accepting the order",
      });
    } finally {
    }
  };

  const rejectOrder = async (orderId: string) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/orders/reject-order/${orderId}`,
        {
          method: "POST",
        }
      );
      const data = await res.json();
      Toast.show({
        type: "customSuccess",
        text1: "Your rejected the order successfully",
      });
      console.log(data);
      fetchOrders();
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "There was an error rejecting the order",
      });
    } finally {
    }
  };

  const dispatchOrder = async (orderId: string) => {
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/orders/dispatch-order/${orderId}`,
        {
          method: "POST",
        }
      );
      const data = await res.json();
      Toast.show({
        type: "customSuccess",
        text1: "You have dispatched the order successfully",
      });
      console.log(data);
      fetchOrders();
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "There was an error dispatching your order",
      });
    } finally {
    }
  };

  return (
    <View className="h-[400]">
      <View className="flex-row gap-4">
        <Pressable
          onPress={() => {
            setActiveTab("pending");
          }}
          className={`border-0.5 border-gray-600 p-2 rounded ${
            activeTab == "pending" ? "bg-[#01b460]" : "bg-white"
          } flex-1`}
        >
          <Text
            className={`text-center ${
              activeTab == "pending" ? "text-white" : "text-black"
            }`}
          >
            Pending
          </Text>
        </Pressable>

        <Pressable
          onPress={() => {
            setActiveTab("ongoing");
          }}
          className={`border-0.5 border-gray-600 p-2 rounded ${
            activeTab == "ongoing" ? "bg-[#01b460]" : "bg-white"
          } flex-1`}
        >
          <Text
            className={`text-center ${
              activeTab == "ongoing" ? "text-white" : "text-black"
            }`}
          >
            Ongoing
          </Text>
        </Pressable>
      </View>

      <View className="mt-4">
        {activeTab == "pending" && (
          <Fragment>
            {loading && (
              <View className="flex-1 items-center justify-center mt-16">
                <ActivityIndicator size="large" color="#01b460" />
              </View>
            )}
            {!loading && pendingOrders.length > 0 && (
              <ScrollView>
                {pendingOrders.map((order: any, index: any) => {
                  return (
                    <OrderItem
                      key={index}
                      order={order}
                      activeTab={"pending"}
                      acceptOrder={acceptOrder}
                      rejectOrder={rejectOrder}
                      dispatchOrder={dispatchOrder}
                    />
                  );
                })}

                <View className="h-12" />
              </ScrollView>
            )}
            {!loading && pendingOrders.length === 0 && (
              <View className="flex-1 items-center justify-center mt-16">
                <Text className="text-2xl font-bold">No Pending Orders</Text>
              </View>
            )}
          </Fragment>
        )}

        {activeTab == "ongoing" && (
          <Fragment>
            {loading && (
              <View className="flex-1 items-center justify-center mt-16">
                <ActivityIndicator size="large" color="#01b460" />
              </View>
            )}
            {!loading && ongoingOrders.length > 0 && (
              <ScrollView>
                {ongoingOrders.map((order: any, index: any) => {
                  return (
                    <OrderItem
                      key={index}
                      order={order}
                      activeTab={"ongoing"}
                      acceptOrder={acceptOrder}
                      rejectOrder={rejectOrder}
                      dispatchOrder={dispatchOrder}
                    />
                  );
                })}

                <View className="h-12" />
              </ScrollView>
            )}

            {!loading && ongoingOrders.length === 0 && (
              <View className="flex-1 items-center justify-center mt-16">
                <Text className="text-2xl font-bold">No Ongoing Orders</Text>
              </View>
            )}
          </Fragment>
        )}
      </View>
    </View>
  );
};

const RestaurantOwnerPage = () => {
  const [dishes, setDishes] = useState<IDish[]>([]);
  const [restaurant, setRestaurant] = useState<IRestaurant>({
    id: "",
    name: "",
    rating: "",
    categories: [],
    address: "",
    image: "",
  });
  const [loading, setLoading] = useState<boolean>(true);
  const [isEditDishVisible, setIsEditDishVisible] = useState<boolean>(false);
  const [isAddingDish, setIsAddingDish] = useState<boolean>(false);

  const userId = useSelector((state: any) => state.auth.id);
  const params = useRoute().params;
  const role = useSelector((state: any) => state.auth.role);
  const restaurantId = (params as any)?.id;

  const [isOrdersVisible, setIsOrdersVisible] = useState<boolean>(false);

  const [dish, setDish] = useState<IDish>({
    id: "",
    dishTitle: "",
    price: 0,
    description: "",
    type: "veg",
    cartQuantity: 0,
    restaurantId: "",
    image: "",
    category: "",
  });

  const fetchRestaurant = async () => {
    try {
      setLoading(true);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/${
          role === "admin" ? "get-restaurant-by-id" : "get-restaurant-by-userId"
        }/${role === "admin" ? restaurantId : userId}`
      );
      const data = await res.json();
      const restaurant: IRestaurant = {
        id: data.id,
        name: data.name,
        rating: data.rating,
        categories: data.categories.split(","),
        address: data.address,
        image: data.image,
      };
      setRestaurant(restaurant);
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  const fetchDishes = async () => {
    try {
      setLoading(true);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-all-dishes-id/${restaurant.id}`
      );
      const data = (await res.json()) as [{ id: string }];
      const dishes = await Promise.all(
        data.map(async (item) => {
          const res = await fetch(
            `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-dish-by-id/${item.id}`
          );
          const data = await res.json();
          return {
            id: data.id,
            dishTitle: data.name,
            price: data.price,
            description: data.description,
            type: data.type,
            cartQuantity: 0,
            restaurantId: data.restaurant,
            image: data.image,
            category: data.category,
          };
        })
      );
      setDishes(dishes);
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  };

  const updateDish = async (dish: IDish) => {
    try {
      setLoading(true);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/update-dish/${dish.id}`,
        {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: dish.dishTitle,
            price: dish.price,
            description: dish.description,
            type: dish.type,
            category: dish.category,
            image: dish.image,
            restaurant: dish.restaurantId,
          }),
        }
      );
      const data = await res.json();
      Toast.show({
        type: "customSuccess",
        text1: "Your dish has been updated successfully",
      });
      console.log(data);
      fetchDishes();
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "There was an error updating your dish",
      });
    } finally {
      setLoading(false);
    }
  };

  const deleteDish = async (dish: IDish) => {
    try {
      setLoading(true);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/delete-dish/${dish.id}`,
        {
          method: "DELETE",
        }
      );
      const data = await res.json();
      Toast.show({
        type: "customSuccess",
        text1: "Your dish has been deleted successfully",
      });
      console.log(data);
      fetchDishes();
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "There was an error deleting your dish",
      });
    } finally {
      setLoading(false);
    }
  };

  const addDish = async (dish: IDish) => {
    try {
      setLoading(true);
      console.log("adding dish", dish);
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/create-dish`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: dish.dishTitle,
            price: dish.price,
            description: dish.description,
            type: dish.type,
            category: dish.category,
            image: dish.image,
            restaurant: restaurant.id,
          }),
        }
      );
      await res.json();
      Toast.show({
        type: "customSuccess",
        text1: "Your dish has been added successfully",
      });
      fetchDishes();
    } catch (err) {
      console.error(err);
      Toast.show({
        type: "customError",
        text1: "There was an error adding your dish",
      });
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchRestaurant();
  }, []);

  useEffect(() => {
    if (restaurant && restaurant.rating) {
      if (!(restaurant.rating.split(".").length > 1)) {
        setRestaurant({ ...restaurant, rating: restaurant.rating + ".0" });
      }
    }
  }, [restaurant]);

  useEffect(() => {
    if (restaurant.id !== "") {
      fetchDishes();
    }
  }, [restaurant]);

  return (
    <SafeAreaView className="flex-1 relative">
      {(!(restaurant.id !== "") || !dishes) && (
        <View className="mt-12">
          <ActivityIndicator size="large" color="rgba(200,0,0, 0.8)" />
        </View>
      )}
      {restaurant.id !== "" && dishes && (
        <>
          <Header restaurantName={restaurant.name} />

          <Pressable
            onPress={() => {
              setIsEditDishVisible(true);
              setIsAddingDish(true);
            }}
            className="p-2 rounded bg-[#01b460] my-4 mx-8"
          >
            <Text className="text-center text-white">Add Dish</Text>
          </Pressable>

          <Body
            dishes={dishes}
            loading={loading}
            fetchDishes={fetchDishes}
            restaurant={restaurant}
            openEditDish={(dish: IDish) => {
              setIsEditDishVisible(true);
              setIsAddingDish(false);
              setDish(dish);
            }}
          />

          <EditDish
            isEditDishVisible={isEditDishVisible}
            setIsEditDishVisible={setIsEditDishVisible}
            isAddingDish={isAddingDish}
            setIsAddingDish={setIsAddingDish}
            dish={dish}
            updateDish={updateDish}
            addDish={addDish}
            deleteDish={deleteDish}
          />
        </>
      )}

      {role == "owner" && !isOrdersVisible && (
        <View className="absolute bottom-0 w-full p-4 bg-white border-t border-gray-200">
          <Button
            title="See Orders"
            onPress={() => {
              setIsOrdersVisible(true);
            }}
          />
        </View>
      )}

      <BottomSheet isVisible={isOrdersVisible}>
        <View className="bg-white p-4">
          <Orders restaurantId={restaurant.id} />
          <Pressable
            onPress={() => {
              setIsOrdersVisible(false);
            }}
            className="border-0.5 border-red-800 p-2 rounded bg-white flex-1 my-4"
          >
            <Text className="text-center">Close</Text>
          </Pressable>
          <View className="h-4" />
        </View>
      </BottomSheet>
    </SafeAreaView>
  );
};

export default RestaurantOwnerPage;

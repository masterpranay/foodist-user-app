import React, { useEffect, useState } from "react";
import { ActivityIndicator, Pressable, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Button, Text } from "../components";
import { SafeAreaView } from "react-native-safe-area-context";
import { setLoginStatus } from "../state/reducers";
import { FlatList, RefreshControl } from "react-native-gesture-handler";
import { Icon } from "@rneui/themed";

const useGetUserOrders = () => {
  const userId = useSelector((state: any) => state.auth.id) || "";
  const [orders, setOrders] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [reload, setReload] = useState<number>(1);

  const fetchUserOrder = async (id: string) => {
    // fetch order info
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/orders/get-order/${id}`
      );
      const data = (await res.json()) as any;

      return data;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const sortOrders = async (orders: any) => {
    orders.sort((a: any, b: any) => {
      return new Date(b.created).getTime() - new Date(a.created).getTime();
    });
  };

  const fetchUserOrders = async () => {
    if (userId == "") return;
    setLoading(true);

    // fetch user orders
    try {
      const res = await fetch(
        `${process.env.EXPO_PUBLIC_API_URL}/api/orders/get-orders-user/${userId}`
      );
      const data = (await res.json()) as [{ id: string }];

      // fetch order info and set into orders
      const orders = await Promise.all(
        data.map((order) => fetchUserOrder(order.id))
      );
      await sortOrders(orders);
      setOrders(orders);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchUserOrders();
  }, [reload]);

  return { orders, loading, setReload, fetchUserOrders };
};

const Header = () => {
  return (
    <View className="bg-white border-b-0.5 border-gray-200 p-2">
      <Text className="text-lg ml-4 text-slate-700" thickness="bold">
        History
      </Text>
    </View>
  );
};

const fetchRestaurantInfo = async (id: string) => {
  try {
    const res = await fetch(
      `${process.env.EXPO_PUBLIC_API_URL}/api/restaurants/get-restaurant-by-id/${id}`
    );
    const data = (await res.json()) as any;

    return {
      name: data.name,
      image: data.image,
    };
  } catch (error) {
    console.log(error);
    return null;
  }
};

const fetchDishInfo = async (id: string) => {
  try {
    const res = await fetch(
      `${process.env.EXPO_PUBLIC_API_URL}/api/dishes/get-dish-by-id/${id}`
    );
    const data = (await res.json()) as any;

    return {
      id: data.id,
      name: data.name,
      price: data.price,
    };
  } catch (error) {
    console.log(error);
    return null;
  }
};

const OrderItem = ({ order }: { order: any }) => {
  const [restaurant, setRestaurant] = useState<any>(null);
  const [dishes, setDishes] = useState<any>([]);
  const [restaurantloading, setRestaurantLoading] = useState<boolean>(false);
  const [dishesloading, setDishesLoading] = useState<boolean>(false);

  useEffect(() => {
    setRestaurantLoading(true);
    setDishesLoading(true);

    const fetchRestaurant = async () => {
      const restaurant = await fetchRestaurantInfo(order.restaurant);
      setRestaurantLoading(false);
      setRestaurant(restaurant);
    };

    const fetchDishes = async () => {
      const dishes = await Promise.all(
        order.dish.map((id: any) => fetchDishInfo(id))
      );
      // add the quantity from order.itemQuantity to dishes
      dishes.forEach((dish: any) => {
        dish.quantity = order.itemQuantity[dish.id];
      });
      setDishesLoading(false);
      setDishes(dishes);
    };

    fetchRestaurant();
    fetchDishes();
  }, []);

  const RestaurantBox = ({ restaurant }: { restaurant: any }) => {
    return (
      <View className="flex flex-row items-center">
        <View className="flex flex-col">
          <Text className="text-slate-700" thickness="bold">
            {restaurant?.name}
          </Text>
        </View>
      </View>
    );
  };

  const DishesBox = ({ dishes }: { dishes: any }) => {
    return (
      <View className="flex flex-col my-2">
        {dishes.map((dish: any, index: any) => (
          <View className="flex flex-row items-center" key={index}>
            <Text className="text-slate-700">{dish?.quantity} x </Text>
            <Text className="text-slate-700">{dish?.name}</Text>
          </View>
        ))}
      </View>
    );
  };

  if (restaurantloading || dishesloading) {
    return (
      <View className="flex flex-col bg-white border-b-0.5 border-gray-200 p-2">
        <ActivityIndicator
          size="large"
          color="#01b460"
          style={{ marginTop: 20 }}
        />
      </View>
    );
  }

  return (
    <View className="flex flex-col bg-white border-0.5 border-gray-200 shadow-lg shadow-gray-400 rounded-lg p-4 m-2">
      <RestaurantBox restaurant={restaurant} />
      <DishesBox dishes={dishes} />
      <View className="border-t-0.5 border-gray-200 py-2">
        <View className="flex flex-row justify-between">
          <Text className="text-slate-700">Total</Text>
          <Text className="text-slate-700">
            &#8377;
            {order?.totalPrice}
          </Text>
        </View>
        <View className="flex flex-row justify-between">
          <Text className="text-slate-700">Status</Text>
          <Text className="text-slate-700">{order.status}</Text>
        </View>

        <View className="flex flex-row justify-between">
          <Text className="text-slate-700">Date: </Text>
          <Text className="text-slate-700">
            {new Date(order.created).toLocaleString()}{" "}
          </Text>
        </View>
      </View>
      {/* <View className="w-full flex-shrink-0">
        <Pressable
          onPress={() => {
            console.log("Reorder!!");
          }}
          className="bg-[#01b460] rounded-lg p-2 flex-row items-center justify-center mt-4 ml-auto"
        >
          <Icon name="reload" type="ionicon" size={16} color="white" />
          <Text className="text-white text-xs ml-2">Reorder</Text>
        </Pressable>
      </View> */}
    </View>
  );
};

const OrderList = ({
  orders,
  loading,
  fetchUserOrders,
}: {
  orders: any;
  loading: boolean;
  fetchUserOrders: any;
}) => {
  return (
    <View className="flex flex-col">
      <FlatList
        data={orders}
        renderItem={({ item }) => <OrderItem order={item} />}
        keyExtractor={(item) => item.id}
        ListFooterComponent={<View className="h-32" />}
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={fetchUserOrders} />
        }
        ListEmptyComponent={
          <Text className="text-slate-700 text-lg text-center mt-4">
            No orders yet
          </Text>
        }
      />
    </View>
  );
};

const History = () => {
  const { orders, loading, setReload, fetchUserOrders } = useGetUserOrders();
  const userId = useSelector((state: any) => state.auth.id) || "";
  const status = useSelector((state: any) => state.auth.status);
  const dispatch = useDispatch();

  if (userId == "" || status !== "loggedin")
    return (
      <SafeAreaView className="h-full w-full">
        <View className="m-auto">
          <Text className="text-slate-700 text-lg">Please login</Text>
          <Button
            title="Login"
            onPress={() => {
              dispatch(setLoginStatus("login"));
            }}
          />
        </View>
      </SafeAreaView>
    );

  return (
    <SafeAreaView>
      <Header />
      {loading && (
        <ActivityIndicator
          size="large"
          color="#01b460"
          style={{ marginTop: 20 }}
        />
      )}

      {!loading && (
        <OrderList
          orders={orders}
          fetchUserOrders={fetchUserOrders}
          loading={loading}
        />
      )}
    </SafeAreaView>
  );
};

export default History;

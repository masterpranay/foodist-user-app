import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Ionicons from "@expo/vector-icons/Ionicons";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import {
  Homepage,
  History,
  Profile,
  RestaurantPage,
  Login,
  Otp,
  Cart,
  AdminHomepage,
  RestaurantOwnerPage,
  Location,
} from "../screens";
import { useEffect, useState } from "react";
import { ActivityIndicator, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import * as SecureStore from "expo-secure-store";
import {
  setLoginStatus,
  setLoginUserId,
  setLoginUserRole,
  setLoginmobileNumber,
} from "../state/reducers";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

interface ILoginStatus {
  status: boolean | string;
  role: string;
}

const useLoginStatus = (): ILoginStatus => {
  const status = useSelector((state: any) => state.auth.status);
  const role = useSelector((state: any) => state.auth.role);

  return {
    status: status,
    role: role,
  };
};

const useGetSavedData = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [data, setData] = useState<any>({});
  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);
    SecureStore.getItemAsync("user").then((data) => {
      if (data) {
        setData(JSON.parse(data));
        dispatch(setLoginUserId(JSON.parse(data).id));
        dispatch(setLoginUserRole(JSON.parse(data).role));
        dispatch(setLoginmobileNumber(JSON.parse(data).mobileNumber));
        dispatch(setLoginStatus(JSON.parse(data).status));
      }
      setLoading(false);
    });
  }, []);

  return {
    loading,
    data,
  };
};

function AppScreens() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: "#fff",
          paddingTop: 10,
          paddingBottom: 10,
          height: 64,
        },
        tabBarLabelStyle: {
          fontFamily: "Inter_500Medium",
          color: "rgba(0,0,0,0.6)",
        },
        tabBarActiveTintColor: "#01b460",
      }}
      sceneContainerStyle={{
        padding: 0,
        backgroundColor: "#fff",
      }}
      initialRouteName="Home"
    >
      <Tab.Screen
        name="Home"
        component={Homepage}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarLabel: "History",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="time" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const ScreenLoader = () => {
  return (
    <View>
      <ActivityIndicator size="large" color="#01b460" />
    </View>
  );
};

const LoginScreens = () => {
  const mobileNumber = useSelector((state: any) => state.auth.mobileNumber);

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          backgroundColor: "#fff",
        },
      }}
      initialRouteName={mobileNumber ? "Otp" : "LoginFirst"}
    >
      <Stack.Screen name="LoginFirst" component={Login} />
      <Stack.Screen name="Otp" component={Otp} />
    </Stack.Navigator>
  );
};

const OtherScreens = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          backgroundColor: "#fff",
        },
      }}
      initialRouteName="First"
    >
      <Stack.Screen name="First" component={AppScreens} />
      <Stack.Screen name="RestaurantPage" component={RestaurantPage} />
      <Stack.Screen name="Cart" component={Cart} />
      <Stack.Screen name="Location" component={Location} />
    </Stack.Navigator>
  );
};

const OnwerScreens = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          backgroundColor: "#fff",
        },
      }}
      initialRouteName="Owner"
    >
      <Stack.Screen name="Owner" component={RestaurantOwnerPage} />
      <Stack.Screen name="History" component={History} />
    </Stack.Navigator>
  );
};

const AdminScreens = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          backgroundColor: "#fff",
        },
      }}
      initialRouteName="Admin"
    >
      <Stack.Screen name="Admin" component={AdminHomepage} />
      <Stack.Screen name="RestaurantOwnerPage" component={RestaurantOwnerPage} />
      <Stack.Screen name="History" component={History} />
    </Stack.Navigator>
  );
};

const HomeStack = () => {
  const loggedinStatus = useLoginStatus();
  const { loading: loadingData, data } = useGetSavedData();

  // SecureStore.deleteItemAsync("user");

  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    setLoading(true);
    if (
      loggedinStatus.status == "skip" ||
      loggedinStatus.status == "loggedin"
    ) {
      setIsLoggedIn(true);
      setLoading(false);
    } else {
      setIsLoggedIn(false);
      setLoading(false);
    }
  }, [loggedinStatus.status]);

  if (loading || loadingData) {
    return (
      <View>
        <ActivityIndicator size="large" color="#01b460" />
      </View>
    );
  }

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          backgroundColor: "#fff",
        },
      }}
      initialRouteName={isLoggedIn ? "Other" : "Login"}
    >
      {!isLoggedIn && <Stack.Screen name="Login" component={LoginScreens} />}
      {isLoggedIn && loggedinStatus.role == "user" && (
        <Stack.Screen name="Other" component={OtherScreens} />
      )}
      {isLoggedIn && loggedinStatus.role == "owner" && (
        <Stack.Screen name="Other" component={OnwerScreens} />
      )}
      {isLoggedIn && loggedinStatus.role == "admin" && (
        <Stack.Screen name="Other" component={AdminScreens} />
      )}
      {isLoggedIn && loggedinStatus.role == "logout" && (
        <Stack.Screen name="Other" component={ScreenLoader} />
      )}
    </Stack.Navigator>
  );
};

export default function Home() {
  return (
    <NavigationContainer>
      <HomeStack />
    </NavigationContainer>
  );
}

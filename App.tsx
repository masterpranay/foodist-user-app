import {
  useFonts,
  Inter_300Light,
  Inter_400Regular,
  Inter_500Medium,
  Inter_700Bold,
  Inter_800ExtraBold,
} from "@expo-google-fonts/inter";
import { StyleSheet, SafeAreaView, View, Text } from "react-native";
import { StatusBar } from "expo-status-bar";
import { Provider } from "react-redux";
import store from "./src/state/store";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import * as SplashScreen from "expo-splash-screen";

import Home from "./src/navigation/navigation";
import Toast, { BaseToast, ErrorToast } from "react-native-toast-message";
import { Icon } from "@rneui/base";

SplashScreen.preventAutoHideAsync().catch(() => {});

export default function App() {
  const [fontsLoaded] = useFonts({
    Inter_300Light,
    Inter_400Regular,
    Inter_500Medium,
    Inter_700Bold,
    Inter_800ExtraBold,
  });

  const toastConfig = {
    customSuccess: ({ text1 }: any) => (
      <View className="w-[90%] flex flex-row items-center justify-center border border-green-400 bg-green-100 rounded p-2">
        {/* <Icon
          name="check"
          size={16}
          color="white"
          reverseColor="#01b460"
          type="antdesign"
          reverse
        /> */}
        <Text
          className="text-slate-600 mr-4 text-sm"
          style={{ fontFamily: "Inter_400Regular" }}
        >
          {text1}
        </Text>
      </View>
    ),
    customError: ({ text1 }: any) => (
      <View className="flex flex-row items-center justify-center border border-red-400 bg-red-100 rounded p-2">
        {/* <Icon
          name="close"
          size={16}
          color="white"
          reverseColor="#01b460"
          type="antdesign"
          reverse
        /> */}
        <Text className="text-slate-600 mr-4 text-sm">{text1}</Text>
      </View>
    ),
  };

  if (!fontsLoaded) {
    return <Text>Loading...</Text>;
  }

  return (
    <Provider store={store}>
      <StatusBar style="auto" animated />
      <SafeAreaView
        className="flex-1 mt-8"
        onLayout={() => {
          SplashScreen.hideAsync().catch(() => {});
        }}
      >
        <GestureHandlerRootView className="flex-1">
          <Home />
          <Toast config={toastConfig} autoHide visibilityTime={5000} />
        </GestureHandlerRootView>
      </SafeAreaView>
    </Provider>
  );
}
